//#region import library
const express = require('express');
const http = require('http');
const { ExpressPeerServer } = require("peer");
//#endregion import library

//#region app
const app = express();
//#endregion app

//#region server
const server = http.createServer(app);
const PORT = process.env.PORT || 5000;
server.listen(PORT, () => console.log(`running on ${PORT}`))
//#endregion server

//#region peerServer
const peerServer = ExpressPeerServer(server, {
    debug: true
  });
app.use('/peerjs', peerServer);
peerServer.on('connection', (client) => { console.log('client connected');});
peerServer.on('disconnect', (client) => { console.log('client disconnected');});
//#endregion peerServer