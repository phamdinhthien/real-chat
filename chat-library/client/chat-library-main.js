
const io = require('socket.io-client');
const Peer = require('peerjs').Peer;
const serverHost = 'pdthien.store';
const serverPort = 3030;
const peerServerPort = 5000;
const serverURL = `https://${serverHost}:${serverPort}`;
const TypeMessageEnum = require('../server/enums/TypeMessageEnum');
_callback = {};
_countRequestSendMsg = 1;
_stream = null;
_currentPeer = null;
_screenStream = null;
_screenSharing = false;
var ACCESS_TOKEN = null;
var peer = null;
var ID_VIDEO_ELEMENT = null;
if(!ID_VIDEO_ELEMENT) {
    ID_VIDEO_ELEMENT = "video-grid";
};
const videoGrid = document.getElementById(ID_VIDEO_ELEMENT);
const myVideo = document.createElement("video");
myVideo.muted = true;

let requestIdStore = [];
let requestIdCallStore = [];
let isTyping = false;
window.currentRoomID = null;
var checkCallVideo = false;

const axios = require('axios').default;
var instance = axios.create({
    baseURL: `${serverURL}/api/v1/`,
    headers: { 'Content-Type': 'application/json' }
});

/**
 * Hàm parst jwt
 * created by pdthien 31.05.2022
 * @param {*} token 
 * @returns 
 */
function parseJwt (token) {
    try {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
        return JSON.parse(jsonPayload);
    } catch (e) {}
};

/**
 * Đối tượng lưu trữ danh sách các hàm sự kiện khai báo từ RealClient.prototype.on
 */
function realManageMethodName() {
    this.lstMethodName = {};
    // Hàm thêm method
    this.putMethod = (methodName, funcValue) => {
        this.lstMethodName[methodName] = funcValue;
    }
    // Hàm lấy method
    this.getMethod = (methodName) => {
        return this.lstMethodName[methodName];
    }
}

/**
 * Đối tượng khởi tạo chính của chương trình
 */
function RealClient() {
    this.socket = null;
    this.socketURL = null;
    this.realManageMethodName = new realManageMethodName();
}

/**
 * Hàm call method từ danh sách hàm thuộc realManageMethodName
 * @param {*} methodName 
 * @param {*} res 
 */
RealClient.prototype.callMethod = function(methodName, res) {
    let method = this.realManageMethodName.getMethod(methodName);
    method && method(res);
}

/**
 * Hàm gắn sự kiện
 * @param {*} methodName 
 * @param {*} funcValue 
 */
RealClient.prototype.on = function(methodName, funcValue) {
    this.realManageMethodName.putMethod(methodName, funcValue);
}

/**
 * Hàm kết nối
 * @param {*} accessToken 
 */
RealClient.prototype.connect = function(accessToken) {
    // khởi tạo interceptors axios
    instance.interceptors.request.use(req => {
        req.headers['Authorization'] = `Bear ${ACCESS_TOKEN}`;
        return req;
    }, err => Promise.reject(err));
    
    instance.interceptors.response.use(async res => {
        let config = res.config;
        if(config && (config.url.includes('/login') || config.url.includes('/refresh-token'))) {
            return res;
        }
        let {status, success, message} = res.data;
        if(!success && status == 401) {
            message = JSON.parse(message);
            if(message.name == "TokenExpiredError") {
                let res = await refreshToken();
                let newAccessToken = res.data.result;
                config.headers['Authorization'] = `Bear ${newAccessToken}`;
                ACCESS_TOKEN = newAccessToken;
                // this.socket && this.socket.disconnect();
                // this.socket = io(this.socketURL, {
                //     query: {
                //         accessToken: ACCESS_TOKEN
                //     }
                // });
                return instance(config);
            }
        }
        return res;
    }, err => Promise.reject(err));

    ACCESS_TOKEN = accessToken;
    // axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;
    // this.socketURL = 'https://chat-library-v1.herokuapp.com/';
    this.socketURL = serverURL;
    this.socket && this.socket.disconnect();
    this.socket = io(this.socketURL, {
        query: {
            accessToken: ACCESS_TOKEN
        }
    });
    let decodeToken = parseJwt(accessToken);

    // Socket kết nối
    this.socket.on('connected', (socketID) => {
        USER_ID = decodeToken.userID;
        decodeToken.socketID = socketID;
        this.socket.emit('access-token', decodeToken);
    })

    this.socket.on('finish-access-token', () => {
        this.callMethod('connected', decodeToken);
    });
    
    // Lỗi kết nối
    this.socket.on('connect_error', (error) => {
        this.callMethod('unauthen');
    })

    // Socket trạng thái tin nhắn
    this.socket.on('handleSendMessageStatus', (res) => {
        if(requestIdStore.includes(res.requestID + res.state))  return;
        requestIdStore.unshift(res.requestID + res.state);
        this.callMethod('watchMsgStatus', res);
        if(!_callback[res.requestID]) {
            return;
        }
        _callback[res.requestID](res);
    });

    // Socket nhận tin nhắn
    this.socket.on('handleSentMessage', (res) => {    
        if(requestIdStore.includes(res.requestID))  return;
        requestIdStore.unshift(res.requestID);
        if(res.senderID != USER_ID) {
            let state = res.roomID == currentRoomID ? 4 : 3;
            this.socket.emit('sendMessageStatus', {roomID: res.roomID, msgID: res.msgID,requestID: res.requestID, state: state});
        }
        this.callMethod('receiveMessage', res);
    })

    // Socket người dùng gõ tin nhắn
    this.socket.on('handleUserBeginTyping', (res) => {
        if(requestIdStore.includes(res.requestID) || res.id == USER_ID)  return;
        requestIdStore.unshift(res.requestID);
        this.callMethod('userBeginTyping', res);
    });

    // Socket người dùng dừng gõ tin nhắn
    this.socket.on('handleUserEndTyping', (res) => {
        if(requestIdStore.includes(res.requestID) || res.id == USER_ID)  return;
        requestIdStore.unshift(res.requestID);
        this.callMethod('userEndTyping', res);
    });

    // Socket người nhận bắt đầu nhận được cuộc gọi
    this.socket.on('handleStartCall', (res) => {
        if(requestIdCallStore.includes(res.requestID)) return;
        requestIdCallStore.unshift(res.requestID);
        !res.noAction && this.callMethod('receiveCall', res);
    });

    // Socket người nhận đồng ý trả lời cuộc gọi
    this.socket.on('handleAggreeAnswer', (res) => {
        if(requestIdCallStore.includes(res.requestID)) return;
        requestIdCallStore.unshift(res.requestID);
        !res.noAction && this.callMethod('receiverAgreeAnswer', res);
        !res.noAction && connectToNewUser(res.userID);
    });

      // Socket người nhận đồng ý trả lời cuộc gọi
      this.socket.on('handleShareScreen', (res) => {
        if(requestIdCallStore.includes(res.requestID)) return;
        requestIdCallStore.unshift(res.requestID);
        !res.noAction && this.callMethod('receiverShareScreen', res);
        !res.noAction && connectToNewUser(res.userID);
    });

    // Socket người nhận từ chối trả lời cuộc gọi
    this.socket.on('handleCancelAnswer', (res) => {
        if(requestIdCallStore.includes(res.requestID)) return;
        requestIdCallStore.unshift(res.requestID);
        this.callMethod('receiverCancelAnswer', res);
        resetCall(); 
    });

    this.socket.on('handleMarkReadMessage', (res) => {
        if(requestIdStore.includes(res.requestID))  return;
        requestIdStore.unshift(res.requestID);
        this.callMethod('receiverMarkReadMsg', res);
    });
    this.socket.on('userStatus', (res) => {
        // if(res.senderID != USER_INFO.)
        this.callMethod('triggerUserStatus', res);
    });

    this.socket.on('handleEditRoomName', (res) => {
        if(requestIdStore.includes(res.requestID))  return;
        requestIdStore.unshift(res.requestID);
        this.callMethod('receiverRenameRoom', res);
        // _callback[requestID] = callbackSendSuccess;
    });

    this.socket.on('handleUndoMsg', (res) => {
        if(requestIdStore.includes('undo-msg-'+ res.msgID))  return;
        requestIdStore.unshift(res.requestID);
        this.callMethod('senderUndoMsg', res);
        // _callback[requestID] = callbackSendSuccess;
    });
    
}

/**
 * Hàm xử lý khi user gõ tin nhắn
 * created by pdthien 16.05.2022
 * @param {*} body 
 * @param {*} callback 
 * @returns 
 */
RealClient.prototype.userBeginTyping = function(roomID) {
    if(!isTyping) {
        let requestID = `begin-typing-${roomID}-${_countRequestSendMsg}-${(new Date()).getTime()}`;
        requestIdStore.unshift(requestID);
        _countRequestSendMsg++;
        this.socket.emit('userBeginTyping', {roomID: roomID, accessToken: ACCESS_TOKEN, requestID});
        isTyping = true;
    }
}

/**
 * Hàm xử lý khi user dừng gõ tin nhắn
 * created by pdthien 16.05.2022
 * @param {*} body 
 * @param {*} callback 
 * @returns 
 */
 RealClient.prototype.userEndTyping = function(roomID) {
    let requestID = `end-typing-${roomID}-${_countRequestSendMsg}-${(new Date()).getTime()}`;
    requestIdStore.unshift(requestID);
    _countRequestSendMsg++;
    this.socket.emit('userEndTyping', {roomID: roomID, accessToken: ACCESS_TOKEN, requestID});
    isTyping = false;
}

/**
 * Hàm gửi tin nhắn
 * created by pdthien 16.05.2022
 * @param {*} body 
 * @param {*} callback 
 * @returns 
 */
RealClient.prototype.sendMessage = function(body, callbackBeforeSend, callbackSendSuccess) {
    if(!body || !body.roomID || !body.userID) return;
    let requestID = `send-msg-${body.roomID}-${body.userID}-${_countRequestSendMsg}-${(new Date()).getTime()}`;
    requestIdStore.unshift(requestID);
    body.requestID = requestID;
    body.accessToken = ACCESS_TOKEN;
    if(!body.type) body.type = TypeMessageEnum.TEXT;
    switch(body.type) {
        case TypeMessageEnum.AUDIO:
            break;
        case TypeMessageEnum.VIDEO:
            break;
        case TypeMessageEnum.PHOTO:
            break;
        case TypeMessageEnum.FILE:
            break;
        case TypeMessageEnum.STICKER:
            break;
        case TypeMessageEnum.CONTACT:
            break;
        case TypeMessageEnum.LOCATION:
            break;
        default:
            body.type = TypeMessageEnum.TEXT;
            break;
    }
    callbackBeforeSend && callbackBeforeSend(body);
    this.socket.emit('sendMessage', body);
    _callback[requestID] = callbackSendSuccess;
    _countRequestSendMsg++;
}

/**
 * Hàm khởi tạo room 1-1
 * created by pdthien 16.05.2022
 * @param {*} body 
 * @param {*} callback 
 */
// RealClient.prototype.initRoom = function(body, callback) {
//     let requestID = `init-room-${body.roomID}-${body.userID}-${_countRequestSendMsg}-${(new Date()).getTime()}`;
//     body.requestID = requestID;
//     body.accessToken = ACCESS_TOKEN;
//     this.socket.emit('sendMessage', body);
//     _callback[requestID] = callback;
//     _countRequestSendMsg++;
// }

/**
 * Bắt đầu sự kiện call audio/video
 * created by pdthien 29.05.2022
 * @param {*} roomID 
 */
RealClient.prototype.startCall = function(roomID, isCallVideo) {
    checkCallVideo = isCallVideo;
    initPeer(() => {
        this.socket.emit("startCall", {roomID, noAction: true, accessToken: ACCESS_TOKEN});
        setTimeout(() => {
            let requestID = `request-call-${roomID}-${_countRequestSendMsg}-${(new Date()).getTime()}`;
            requestIdCallStore.unshift(requestID);
            this.socket.emit("startCall", {roomID, accessToken: ACCESS_TOKEN, requestID, isCallVideo});
            _countRequestSendMsg++;
        }, 1000)
    });
}

/**
 * Đồng ý trả lời audio/video
 * created by pdthien 29.05.2022
 * @param {*} res 
 */
RealClient.prototype.aggreeAnswer = function(roomID, isCallVideo = false) {
    checkCallVideo = isCallVideo;
    initPeer(() => {
        this.socket.emit("aggreeAnswer", {roomID: roomID, accessToken: ACCESS_TOKEN, noAction: true});
        setTimeout(() => {
            let requestID = `answer-call-${roomID}-${_countRequestSendMsg}-${(new Date()).getTime()}`;
            requestIdCallStore.unshift(requestID);
            this.socket.emit("aggreeAnswer", {roomID: roomID, accessToken: ACCESS_TOKEN, requestID, isCallVideo});
            _countRequestSendMsg++;
        }, 1000)
    });
}

RealClient.prototype.shareScreen = function(roomID) {
    var displayMediaOptions = {
        video: {
            cursor: "always"
        },
        audio: true
    };
    
    navigator.mediaDevices.getDisplayMedia(displayMediaOptions)
    .then((stream) => {
        _screenStream = stream;
        let videoTrack = _screenStream.getVideoTracks()[0];
        videoTrack.onended = () => {
            stopScreenSharing()
        }
        if (peer) {
            let sender = _currentPeer.peerConnection.getSenders().find(function (s) {
                return s.track.kind == videoTrack.kind;
            })
            sender.replaceTrack(videoTrack)
            _screenSharing = true
        }
    });

    function stopScreenSharing() {
        if (!_screenSharing) return;
        let videoTrack = _stream.getVideoTracks()[0];
        if (peer) {
            let sender = _currentPeer.peerConnection.getSenders().find(function (s) {
                return s.track.kind == videoTrack.kind;
            })
            sender.replaceTrack(videoTrack)
        }
        _screenStream.getTracks().forEach(function (track) {
            track.stop();
        });
        _screenSharing = false
    }
}



/**
 * Từ chối cuộc gọi
 * created by pdthien 27.08.2022
 * @param {*} roomID 
 */
RealClient.prototype.cancelAnswer = function(roomID) {
    let requestID = `cancel-call-${roomID}-${_countRequestSendMsg}-${(new Date()).getTime()}`;
    requestIdCallStore.unshift(requestID);
    this.socket.emit("cancelAnswer", {roomID: roomID, accessToken: ACCESS_TOKEN, requestID});
    _countRequestSendMsg++;
    resetCall();
}

function resetCall() {
    if(!_stream) return;
    _stream.getVideoTracks().forEach(function(track) {
        track.enabled = false;
    });
    _stream.getAudioTracks().forEach(function(track) {
        track.enabled = false;
    });
    _stream.getTracks().forEach(function(track) {
        track.stop();
    });
}

RealClient.prototype.toggleMuteAudio = function() {
    _stream.getAudioTracks().forEach(function(track) {
        track.enabled = !track.enabled;
    });
}

RealClient.prototype.toggleMuteVideo = function() {
    navigator.mediaDevices.getUserMedia({
        audio: true,
        video: true
    }).then((stream) => {
        addVideoStream(myVideo, stream);
        _stream = stream;
    });
    _stream.getVideoTracks().forEach(function(track) {
        track.enabled = !track.enabled;
    });
}

/**
 * Hàm upload file
 * created by pdthien
 * @param {*} data 
 * @returns 
 */
 RealClient.prototype.uploadFile = function(data) {
    return new Promise(function(resolve, reject) {
        fetch('https://pdthien.store:4000/api/v1/file/upload-file', {
            method: 'POST',
            headers: {
                'authorization': 'Bearer ' + ACCESS_TOKEN
            },
            body: data,
        })
        .then(response => response.json())
        .then((data) => {
            resolve(data);
        })
        .catch(function(error) {
            reject(error);
        });
    });
}

/**
 * Hàm lấy danh sách room
 * created pdthien
 * @param {*} payload 
 * @param {*} callback 
 */
RealClient.prototype.getListRoom = function(payload, callback) {
    !payload && (payload = {});
    instance.post('room/get-paging', {
        pageIndex: payload.pageIndex || 1,
        pageSize: payload.pageSize || 25,
        ...payload
    }).then(res => {
        callback && callback(res.data);
    }).catch(err => console.log(err))
}

/**
 * Hàm lấy danh dách user
 * @param {*} payload 
 * @param {*} callback 
 */
RealClient.prototype.getListUser = function(payload, callback) {
    !payload && (payload = {});
    instance.post('user/get-paging', {
        pageIndex: payload.pageIndex || 1,
        pageSize: payload.pageSize || 25,
        ...payload
    }).then(res => {
        callback && callback(res.data);
    }).catch(err => console.log(err))
}

RealClient.prototype.registerUser = function(payload, callback) {
    instance.post('auth/register', payload).then(res => {
        callback && callback(res.data);
    }).catch(err => console.log(err))
}

function refreshToken() {
    return instance.post('auth/refresh-token', {});
}

RealClient.prototype.login = function(payload, callback) { 
    instance.post('auth/login', payload).then(res => {
        setLocalStorage('accessToken', res.data.result);
        callback && callback(res.data);
    }).catch(err => console.log(err))
}

function getLocalStorage(key) {
    return localStorage.getItem(`realClient_${key}`);
}

function setLocalStorage(key, value) {
    localStorage.setItem(`realClient_${key}`, value);
}

/**
 * Hàm lấy danh sách msg
 * created by pdthien
 * @param {*} payload 
 * @param {*} callback 
 */
RealClient.prototype.getListMsg = function(payload, callback) {
    !payload && (payload = {});
    instance.post('message/get-paging', {
        pageIndex: payload.pageIndex || 1,
        pageSize: payload.pageSize || 25,
        filter: `[["roomID","=",${payload.roomID}]]`,
        sort: {msgID: -1},
        ...payload
    }).then(res => {
        callback && callback(res.data);
    }).catch(err => console.log(err))
}

/**
 * Nhắn tin 1-1
 * created by pdthien 16.10.2022
 * @param {*} receiverID 
 * @param {*} callback 
 */
RealClient.prototype.getRoomByUserID = function(receiverID, callback) {
    instance.post('message/get-paging-by-userid', {
        "receiverID": receiverID,
        "socketID": this.socket.id
    }).then(res => {
        let data = res.data;
        if(data.result.type == 1) {
            data.result.usersInfo.forEach(user => user.id = user.userID);
            data.result.roomInfo.users = data.result.usersInfo;
        } else {
            data.result.roomInfo = data.result.data[0];
            delete data.result.data;
        }
        callback && callback(res.data);
    }).catch(err => console.log(err))
}

/**
 * Hàm xử lý xoá member
 * created by pdthien 23.10.2022
 * @param {*} roomID 
 * @param {*} userID 
 * @param {*} callback 
 */
RealClient.prototype.deleteUserInRoom = function(roomID, userID, callback) {
    instance.post('room/delete-user', {
        "userID": userID,
        "roomID": roomID
    }).then(res => {
        callback && callback(res.data);
    }).catch(err => console.log(err))
}

/**
 * Hàm xoá tin nhắn
 * created by pdthien 23.05.2022
 * @param {*} roomID 
 * @param {*} msgID 
 * @param {*} callback 
 */
RealClient.prototype.deleteMsg = function(roomID, msgID, callback) {
    instance.post('message/delete-msg', {
        "msgID": msgID,
        "roomID": roomID
    }).then(res => {
        callback && callback(res.data);
    }).catch(err => console.log(err))
}

/**
 * Hàm thu hồi tin nhắn
 * created by pdthien 23.05.2022
 * @param {*} roomID 
 * @param {*} msgID 
 * @param {*} callback 
 */
RealClient.prototype.undoMsg = function(roomID, msgID, callback) {
    instance.post('message/undo-msg', {
        "msgID": msgID,
        "roomID": roomID,
        "socketID": this.socket.id
    }).then(res => {
        requestIdStore.unshift('undo-msg-'+ msgID);
        callback && callback(res.data);
    }).catch(err => console.log(err))
}


/**
 * Hàm xử lý thêm member
 * created by pdthien 23.10.2022
 * @param {*} roomID 
 * @param {*} userID 
 * @param {*} callback 
 */
RealClient.prototype.addUserInRoom = function(roomID, userIDs, callback) {
    instance.post('room/add-user', {
        "userIDs": userIDs,
        "roomID": roomID
    }).then(res => {
        callback && callback(res.data);
    }).catch(err => console.log(err))
}

/**
 * Hàm xử lý sửa tên nhóm
 * created by pdthien 23.10.2022
 * @param {*} roomID 
 * @param {*} userID 
 * @param {*} callback 
 */
RealClient.prototype.editRoomName = function(roomID, newRoomName, callback) {
    let requestID = `edit-room-name-${roomID}-${_countRequestSendMsg}-${(new Date()).getTime()}`;
    requestIdStore.unshift(requestID);
    this.socket.emit("editRoomName", {roomID: roomID, newRoomName, accessToken: ACCESS_TOKEN, requestID});
    _countRequestSendMsg++;
}

/**
 * Hàm tạo room 1-n
 * created by pdthien 16.10.2022
 * @param {*} receiverIDs 
 * @param {*} callback 
 */
RealClient.prototype.createRoomGroup = function(receiverIDs, callback) {
    instance.post('room/create-room-group', {
        "receiverIDs": receiverIDs,
        "socketID": this.socket.id
    }).then(res => {
        let data = res.data;
        data.result.usersInfo.forEach(user => user.id = user.userID);
        data.result.roomInfo.users = data.result.usersInfo;
        callback && callback(res.data);
    }).catch(err => console.log(err))
}

/**
 * Đánh dấu đã đọc tin nhắn
 * @param {*} roomID 
 */
RealClient.prototype.markReadMessage = function(roomID) {
    let requestID = `mark-msg-${roomID}-${_countRequestSendMsg}-${(new Date()).getTime()}`;
    requestIdStore.unshift(requestID);
    this.socket.emit("markReadMessage", {roomID: roomID, accessToken: ACCESS_TOKEN, requestID});
    _countRequestSendMsg++;
}

RealClient.prototype.editMessage = function() {

}

/**
 * Kết nối call tới user khác
 * created by pdthien 29.05.2022
 * @param {*} userId 
 */
 const connectToNewUser = (userId) => {
    const call = peer.call(userId.toString(), _stream);
    const video = document.createElement("video");
    call.on("stream", (userVideoStream) => {
      addVideoStream(video, userVideoStream);
    });
    _currentPeer = call;
};

/**
 * Thêm video call
 * created by pdthien 29.05.2022
 * @param {*} video 
 * @param {*} stream 
 */
const addVideoStream = (video, stream) => {
    video.srcObject = stream;
    video.addEventListener("loadedmetadata", () => {
        video.play();
        videoGrid.append(video);
        let videos = document.getElementById('video-grid').querySelectorAll('video');
        let widthVideo = 100/videos.length + '%';
        videos.forEach(video => {
            video.style.width = widthVideo;
        })
    });
}

/**
 * Hàm khởi tạo peer
 * created by pdthien 29.05.2022
 */
function initPeer(cb = null) {
    if(!peer) {
        peer = new Peer(USER_ID, {
            host: serverHost,
            port: peerServerPort,
            path: '/peerjs'
        });
        // peer = new Peer(USER_ID, {
        //     host: 'peer-server-v1.herokuapp.com',
        //     port: 443,
        //     path: '/peerjs',
        //     key: 'peerjs',
        //     secure: true
        // });
    } else {
        navigator.mediaDevices.getUserMedia({
            audio: true,
            video: checkCallVideo
        }).then((stream) => {
            if(cb) cb();
            addVideoStream(myVideo, stream);
            _stream = stream;
        });
        return;
    }
    
    peer.on("open", (id) => {
        console.log("Peer open: " + id);
        // khởi tạo camera
        navigator.mediaDevices.getUserMedia({
            audio: true,
            video: checkCallVideo
        }).then((stream) => {
            if(cb) cb();
            addVideoStream(myVideo, stream);
            peer.on("call", (call) => {
                _currentPeer = call;
                call.answer(stream);
                const video = document.createElement("video");
                call.on("stream", (userVideoStream) => {
                    addVideoStream(video, userVideoStream);
                });
            })
            _stream = stream;
        });
     });
    
    peer.on('error', function(err){
        console.log('Peer error: ' + JSON.stringify(err));
    });
}

window.RealClient = RealClient;