module.exports = {
    onSuccess: (res, data, status = 200) => {
        const resdata = {
            status: status,
            success: true
        };
        if(data) resdata.result = data;
        return res.status(status).json(resdata);
    },
    onError: (res, message, status, codeError) => {
        const resdata = {
            status: status || 500,
            success: false,
            message: JSON.stringify(message),
            codeError
        }
        return res.status(200).json(resdata);
    },
    validationError: (res, message, data) => {
        const resdata = {
            status: 400,
            success: false,
            message: String(message)
        }
        if(data) resdata.result = data;
        return res.status(200).json(resdata)
    }
}