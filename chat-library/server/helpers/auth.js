const jwt = require('jsonwebtoken');
var AccessKeyModel = require('../models/AccessKeyModel');

module.exports = {
    /**
     * Hàm tạo jwt (accessToken, refreshToken)
     * created by pdthien 19.04.2022
     * @param {*} data 
     * @returns 
     */
    createToken: async(data) => {
        let tenantID = data.tenantID;
        let accessKey = await AccessKeyModel.findOne({tenantID});
        let accessToken = jwt.sign(data, accessKey.keySecret, {expiresIn: '12h'});
        let refreshToken = jwt.sign(data, process.env.REFRESH_TOKEN_SECRET, {expiresIn: '12h'});
        return { accessToken, refreshToken };
    }
}