//#region import file
require('./global');
//#endregion import file

//#region import library
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const helmet = require("helmet");
const morgan = require('morgan');
const compression = require('compression');
//#endregion import library

//#region app
const app = express();
app.use(
  helmet({
    contentSecurityPolicy: false
 })
);
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());
app.use(morgan("dev"));
app.use(compression());
dotenv.config();
//#endregion app

//#region server
const server = http.createServer(app);
const PORT = process.argv[2] || process.env.PORT || 3030;
server.listen(PORT, () => console.log(`running on ${PORT}`))
//#endregion server

//#region mongdoDB
require('./configs/mongoose.config');
//#endregion mongdoDB

//#region socket
var io = require('./configs/socket.config')(server);
const socketEvent = require('./socket/socket.event');
socketEvent(io)
//#endregion

//#region redis
const redisEvent = require('./redis/redis.event');
redisEvent(io);
//#endregion

//#region peer server
// const peerServer = require('./configs/peer.config');
// peerServer(server, app);
//#endregion

//#region Router
const router = require('./routers/index.router');
app.use('/api/v1', router);
//#endregion

// handle error
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  const statusCode = err.status || 500;
  return res.status(statusCode).json({
      status: 'error',
      code: statusCode,
      message: err.message || 'Internal Server Error'
  });
});