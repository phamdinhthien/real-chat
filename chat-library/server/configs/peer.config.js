const { ExpressPeerServer } = require("peer");

module.exports = function (server, app) {

    const peerServer = ExpressPeerServer(server, {
        debug: true
    });
    app.use('/peerjs', peerServer);
    peerServer.on('connection', (client) => { console.log('client connected'); });
    peerServer.on('disconnect', (client) => { console.log('client disconnected'); });
}