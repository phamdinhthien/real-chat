var mongoose = require('mongoose');
mongoose.set('strictQuery', true);

mongoose.connect(process.env.URL_MONGDO_DB || "mongodb://mongo:27017/chat-real", {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

mongoose.connection.on('connected', () => {
    console.log('Mongo has connected succesfully')
})
mongoose.connection.on('reconnected', () => {
    console.log('Mongo has reconnected')
})
mongoose.connection.on('error', error => {
    console.log('Mongo connection has an error', error)
    mongoose.disconnect()
})
mongoose.connection.on('disconnected', () => {
    console.log('Mongo connection is disconnected')
});

module.exports = mongoose;