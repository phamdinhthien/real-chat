const socketio = require('socket.io');

module.exports = function(server) {
    var io = socketio(server, {
        cors: {
          origin: '*',
        }
    });
    return io;
}