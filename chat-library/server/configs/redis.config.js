const { createClient } = require("redis");

var pubClient = createClient({ url: "redis://redis:6379" });
var subClient = pubClient.duplicate();

module.exports = { pubClient, subClient };