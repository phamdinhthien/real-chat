module.exports = {
    INIT: 1,
    TEXT: 2,
    AUDIO: 3,
    VIDEO: 4,
    PHOTO: 5,
    FILE: 6,
    LINK: 7,
    STICKER: 8,
    CONTACT: 9,
    LOCATION: 10,
    RENAME_ROOM: 11,
}