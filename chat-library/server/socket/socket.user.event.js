var UserModel = require('../models/UserModel');
var RoomModel = require('../models/RoomModel');
var MessageModel = require('../models/MessageModel');
var stateMessageEnum = require('../enums/StateMessageEnum');
var eventBus = require('../emitter/eventBus');
const TypeMessageEnum = require('../enums/TypeMessageEnum');
const { encryptData } = require('../helpers/encryption');

module.exports = function(socket, pubClient, subClient) {

  /**
   * Hàm gửi tin nhắn
   * created by pdthien 18.06.2022
   * @param {*} senderID 
   * @param {*} roomID 
   * @param {*} msgObj 
   */
  async function sendMessageToRoom(senderID, roomID, msgObj) {
    try {
      let sender = await UserModel.getByID(senderID);
      let senderName = sender.name;
      let content = encryptData(msgObj.content);
      // tạo message
      let payloadMsg = {
        roomID: roomID,
        senderID: senderID,
        senderName: senderName,
        type: msgObj.type,
        state: stateMessageEnum.SENT,
        content: content,
        requestID: msgObj.requestID,
        tenantID: msgObj.tenantID
      }
      let newMsg = await MessageModel.create(payloadMsg);
      // update lastMsgID cho room
      lstMsg = {
        lastMsgID: newMsg.msgID,
        lastMsgContent: content,
        lastMsgType: msgObj.type,
        lastMsgSender: {
          userID: senderID,
          userName: senderName
        }
      }
      await RoomModel.updateByID(roomID, lstMsg);
      return newMsg;
    } catch (err) {
      throw err;
    }
  }

  /**
   * Hàm xử lý việc gửi msg tới room
   * created by pdthien 18.05.2022
   * @param {*} socket 
   * @param {*} roomID 
   * @param {*} eventName 
   * @param {*} objectSend 
   * @param {*} senderID 
   */
  async function handleSendToRoom(socket, roomID, eventName, objectSend, senderID) {
    let socketID = socket.id;
    pubClient.publish('channel', JSON.stringify({roomID, eventName, objectSend, senderID, socketID}));
  }

  function handleEventBusSocket({roomID, eventName, objectSend}) {
    switch (eventName) {
      case 'initRoomEventBus':
        handleSendToRoom(socket, roomID, 'handleSentMessage', objectSend, USER_ID);
        socket.emit('handleSendMessageStatus', {requestID: objectSend.requestID, state: stateMessageEnum.SENT, msgID: objectSend.msgID, senderID: objectSend.senderID});
        break;
      case 'undoMsg':
        handleSendToRoom(socket, roomID, 'handleUndoMsg', {
          roomID: objectSend.roomID,
          msgID: objectSend.msgID
        });
        break;
      default:
        break;
    }
  }
  // Nhận xử kiện khởi tạo room để bắn socket
  eventBus.eventBus.on(`eventBusSocket_${socket.id}`, handleEventBusSocket);
  
  var USER_INFO = null;
  var USER_ID = null;
  var TENANT_ID = null;
  //#region authorization
  socket.on('access-token', async (req) => {
    USER_ID = req.userID;
    TENANT_ID = req.tenantID;
    let user = await UserModel.getByID(USER_ID);
    let socketList = user.socketIDs;
    socketList.push(req.socketID);
    let payload = {
          userID : USER_ID,
          data: {
              socketIDs: socketList,
              isOnline: true
          }
      }
      socket.join(user.tenantID);
      USER_INFO = await UserModel.updateByID(USER_ID, payload.data);
      handleSendToRoom(socket, user.tenantID, 'userStatus', {senderID: USER_ID, isOnline: true}, USER_ID);
      socket.emit('finish-access-token');
  });
  //#endregion

  //#region gửi tin nhắn
  socket.on('sendMessage', async function(payload) {
    let senderID = USER_ID;
    let roomID = payload.roomID;
    if(roomID) {
      let msgObj = {
        content: payload.content,
        requestID: payload.requestID,
        type: payload.type,
        tenantID: TENANT_ID
      }
      let newMsg = await sendMessageToRoom(senderID, roomID, msgObj);
      let sender = await UserModel.getByID(senderID);
      let senderName = sender.name;
      handleSendToRoom(socket, roomID, 'handleSentMessage', {senderName, content: payload.content, requestID: payload.requestID, roomID, senderID, createdAt: newMsg.createdAt, msgID: newMsg.msgID, type: newMsg.type}, USER_ID);
      socket.emit('handleSendMessageStatus', {requestID: payload.requestID, state: stateMessageEnum.SENT, msgID: newMsg.msgID, senderID});
    } else {
      // let receiverID = payload.receiverID;
      // let isGroup = payload.isGroup;
      // RoomModel.initRoom(receiverID, isGroup, USER_ID, TENANT_ID);
    }
  })
  //#endregion

  //#region gửi trạng thái tin nhắn
  socket.on('sendMessageStatus', async function(res) {
    let payload = {state: res.state};
    if(res.state == stateMessageEnum.SEEN) {
      let doc = await MessageModel.getByID(res.msgID);
      if(doc.senderID == USER_ID) {
        handleSendToRoom(socket, res.roomID, 'handleSendMessageStatus', {requestID: res.requestID, state: stateMessageEnum.SENT, msgID: res.msgID, readBy: payload.readBy, id: USER_ID, senderID: doc.senderID}, USER_ID);
        return;
      }
      let readBy = doc.readBy.filter(e => e.readByUserID).map(e => e.readByUserID);
      readBy.push({readByUserID: USER_ID})
      payload.readBy = readBy;
    }
    let doc = await MessageModel.updateByID(res.msgID, payload);
    handleSendToRoom(socket, res.roomID, 'handleSendMessageStatus', {requestID: res.requestID, state: res.state, msgID: res.msgID, readBy: payload.readBy, id: USER_ID, senderID: doc.senderID}, USER_ID);
  })
  //#endregion

  //#region user nhập tin nhắn
  socket.on('userBeginTyping', async function(payload) {
    handleSendToRoom(socket, payload.roomID, 'handleUserBeginTyping', {requestID: payload.requestID, roomID: payload.roomID, id: USER_ID, name: USER_INFO.name}, USER_ID);
  })
  //#endregion

  //#region user ngừng nhập tin nhắn
  socket.on('userEndTyping', async function(payload) {
    handleSendToRoom(socket, payload.roomID, 'handleUserEndTyping', {requestID: payload.requestID, roomID: payload.roomID, id: USER_ID, name: USER_INFO.name}, USER_ID);
  })
  //#endregion

  //#region đánh dấu đọc tin nhắn
  socket.on('markReadMessage', async function(payload) {
    if(!USER_INFO) return;
    let roomID = payload.roomID;
    let currentUserID = payload.userID || USER_ID;
    await MessageModel.markReadMessage(roomID, currentUserID);
    handleSendToRoom(socket, payload.roomID, 'handleMarkReadMessage', {requestID: payload.requestID, roomID: payload.roomID, id: USER_ID, name: USER_INFO.name}, USER_ID);
  })
  //#endregion

  //#region đánh dấu đã đọc tin nhắn
  socket.on('markReadRoom', async function(payload) {
  })
  //#endregion

  //#region bắt đầu cuộc gọi
  socket.on('startCall', async function(payload) {
    let roomID = payload.roomID;
    handleSendToRoom(socket, roomID, 'handleStartCall', {roomID: roomID, userID: USER_ID, noAction: payload.noAction, requestID: payload.requestID, isCallVideo: payload.isCallVideo}, USER_ID);
  })
  //#endregion

  //#region đồng ý cuộc gọi
  socket.on('aggreeAnswer', async function(payload) {
    let roomID = payload.roomID;
    handleSendToRoom(socket, roomID, 'handleAggreeAnswer', {roomID: roomID, userID: USER_ID, noAction: payload.noAction, requestID: payload.requestID, isCallVideo: payload.isCallVideo}, USER_ID);
  })
  //#endregion

  //#region từ chối cuộc gọi
  socket.on('cancelAnswer', async function(payload) {
    let roomID = payload.roomID;
    handleSendToRoom(socket, roomID, 'handleCancelAnswer', {roomID: roomID, userID: USER_ID, requestID: payload.requestID}, USER_ID);
  })
  //#endregion

  //#region chia sẻ màn hình
  socket.on('shareScreen', async function(payload) {
    let roomID = payload.roomID;
    handleSendToRoom(socket, roomID, 'handleShareScreen', {roomID: roomID, userID: USER_ID, noAction: payload.noAction, requestID: payload.requestID, isCallVideo: payload.isCallVideo}, USER_ID);
  })
  //#endregion

  //#region bắt đầu cuộc gọi
  socket.on('editRoomName', async function(payload) {
    let roomID = payload.roomID;
    let newRoomName = payload.newRoomName;
    await RoomModel.updateByID(roomID, {name: newRoomName});
    if(roomID) {
      let msgObj = {
        content: newRoomName,
        requestID: payload.requestID,
        type: TypeMessageEnum.RENAME_ROOM,
        tenantID: TENANT_ID
      }
      let newMsg = await sendMessageToRoom(USER_INFO.userID, roomID, msgObj);
      handleSendToRoom(socket, roomID, 'handleSentMessage', {roomID, senderName: USER_INFO.name, requestID: payload.requestID, senderID: USER_INFO.userID, createdAt: newMsg.createdAt, msgID: newMsg.msgID, type: newMsg.type, content: newRoomName}, USER_ID);
      socket.emit('handleSendMessageStatus', {requestID: payload.requestID, state: stateMessageEnum.SENT, msgID: newMsg.msgID, senderID: USER_INFO.userID});
    }
})
  //#endregion
  

  socket.on('disconnect', async () => {
    eventBus.eventBus.removeListener(`eventBusSocket_${socket.id}`, handleEventBusSocket);
    // subClient.unsubscribe('channel');
    if(!USER_ID) return;
    let user = await UserModel.getByID(USER_ID);
    let socketList = user.socketIDs;
    if(socketList.indexOf(socket.id) < 0) return;
    socketList.splice(socketList.indexOf(socket.id), 1);
    let payload = {
        userID: USER_ID,
        data: {
            socketIDs: socketList,
            isOnline: false
        }
    }
    handleSendToRoom(socket, user.tenantID, 'userStatus', {senderID: USER_ID, isOnline: false}, USER_ID);
    UserModel.updateByID(USER_ID, payload.data);
})
}