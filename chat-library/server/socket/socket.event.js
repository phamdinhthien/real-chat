const authMiddleware = require('../middlewares/auth');
const { pubClient, subClient } = require('../configs/redis.config');

module.exports = function(io) {
    io.use(async (socket, next) => {
        let query = socket.handshake.query;
        if (query && query.accessToken){
          let authResult = await authMiddleware.verifyAccessTokenForSocket(query.accessToken);
          if(authResult.status != 200) {
            next(new Error(authResult.code));
          } else {
            next();
          }
        } else next(new Error("invalid"));
    })
    
    var userEvent = require('./socket.user.event');
    io.on('connection', socket => {
        console.log('socket connection established ' + socket.id)
        socket.emit('connected', socket.id);
        userEvent(socket, pubClient, subClient);
    })
}
