const { pubClient, subClient } = require('../configs/redis.config');
const { createAdapter } = require("@socket.io/redis-adapter");

var RoomModel = require('../models/RoomModel');
var UserModel = require('../models/UserModel');

module.exports = function(io) {
  async function subscribeOtherUser(roomID, otherUserId, exceptSocketID) {
    let users = await UserModel.getListByIDs(otherUserId);
    users.map((u) => {
      u && u.socketIDs && u.socketIDs.forEach(socketID => {
        let sockets = io.sockets.sockets;
        const socketConn = sockets.has(socketID);
        if (socketConn) {
          if(socketID == exceptSocketID) {
            sockets.get(socketID).leave(roomID);
            return;
          }
          sockets.get(socketID).join(roomID);
        }
      })
    });
}
  subClient.on('message', async (channel, message) => {
      let data = JSON.parse(message);
      let eventName = data.eventName;
      if(eventName === "userStatus") {
        io.to(data.roomID).emit(data.eventName, data.objectSend);
        return;
      }
      let room = await RoomModel.getByID(data.roomID);
      if(!room) return;
      let receivers = room.userIDs;
      await subscribeOtherUser(data.roomID, receivers, data.socketID);
      setTimeout(() => {
          io.to(data.roomID).emit(data.eventName, data.objectSend);
      }, 100)
    });
    
  subClient.on("error", (err) => {
    console.log(err);
  });
  pubClient.on("error", (err) => {
    console.log(err);
  });


  io.adapter(createAdapter(pubClient, subClient));
  subClient.subscribe('channel', (err, count) => {
    if (err) {
      console.error('Failed to subscribe to channel', err);
    } else {
      console.log(`Subscribed to ${count} channels`);
    }
  });

}
