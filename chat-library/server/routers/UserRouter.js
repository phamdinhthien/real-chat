const express = require('express');
const router = express.Router();

const userController = require('../controllers/UserController');
const controller = new userController();
const authMiddleware = require('../middlewares/auth');

router.post('/get-paging', authMiddleware.verifyAccessToken, controller.getPaging);
router.post('/save', authMiddleware.verifyAccessToken, controller.save);
router.post('/save-list', authMiddleware.verifyAccessToken, controller.saveList);
router.post('/update-field', authMiddleware.verifyAccessToken, controller.updateField);

module.exports = router;