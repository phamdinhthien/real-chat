const express = require('express');
const router = express.Router();

const msgController = require('../controllers/MessageController');
const controller = new msgController();

const authMiddleware = require('../middlewares/auth');

router.post('/get-paging', authMiddleware.verifyAccessToken, controller.getPaging);
router.post('/get-paging-by-userid', authMiddleware.verifyAccessToken, controller.getMsgByUser);
router.post('/delete-msg', authMiddleware.verifyAccessToken, controller.deleteMsg);
router.post('/undo-msg', authMiddleware.verifyAccessToken, controller.undoMsg);

module.exports = router;