const express = require('express');
const router = express.Router();

const roomController = require('../controllers/RoomController');
const controller = new roomController();

const authMiddleware = require('../middlewares/auth');

router.post('/get-paging', authMiddleware.verifyAccessToken, controller.getPaging);
router.post('/create-room-group', authMiddleware.verifyAccessToken, controller.createRoomGroup);
router.post('/save-list', authMiddleware.verifyAccessToken, controller.saveList);
router.post('/delete-user', authMiddleware.verifyAccessToken, controller.deleleUser);
router.post('/add-user', authMiddleware.verifyAccessToken, controller.addUser);

module.exports = router;