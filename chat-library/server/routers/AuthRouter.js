const express = require('express');
const router = express.Router();
const AuthController = require('../controllers/AuthController');
const controller = new AuthController();
const authMiddleware = require('../middlewares/auth');

router.post('/register', authMiddleware.verifyAccessToken, controller.registerUser);
router.post('/login', controller.login);
router.post('/refresh-token', controller.refreshToken);

module.exports = router;