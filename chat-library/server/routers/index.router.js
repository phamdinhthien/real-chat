const express = require('express');
const router = express.Router();

const authRouter = require('./AuthRouter');
const userRouter = require('./UserRouter');
const roomRouter = require('./RoomRouter');
const msgRouter = require('./MessageRouter');
const keyRouter = require('./AccessKeyRouter');

router.use('/auth', authRouter);
router.use('/user', userRouter);
router.use('/room', roomRouter);
router.use('/message', msgRouter);
router.use('/key', keyRouter);

module.exports = router;