const express = require('express');
const router = express.Router();

const accessKeyController = require('../controllers/AccessKeyController');
const controller = new accessKeyController();

router.get('/get-new-access-key', controller.genAccessKey);

module.exports = router;