
module.exports = function(model, idName) {
    /**
     * Hàm tạo bản ghi
     * created by pdthien 30/07/2022
     * @param {*} payload 
     * @returns 
     */
    model.statics.createOne = async function(payload) {
        try {
            const result = this.create(payload);
            if (!result) return null;
            return result;
        } catch (error) {
            throw error;
        }
    }
    /**
     * Hàm lấy bản ghi theo ID
     * created by pdthien 21.05.2022
     * @param {*} ID 
     * @returns 
     */
    model.statics.getByID = async function (ID, selectColumns = null) {
        try {
            let query = {};
            query[idName] = ID;
            const result = await this.findOne(query).select(selectColumns).lean();
            if (!result) return null;
            return result;
        } catch (error) {
          throw error;
        }
    }
    /**
     * Hàm lấy danh sách bản ghi theo id
     * created by pdthien 21.05.2022
     * @param {*} IDs 
     * @returns 
     */
    model.statics.getListByIDs = async function (IDs, selectColumns = null) {
        try {
            let query = {};
            query[idName] = {$in: IDs};
            const result = await this.find(query).select(selectColumns).lean();
            if (!result) throw ({ error: 'No record with this id found' });
            return result;
        } catch (error) {
            throw error;
        }
    }
    /**
     * Hàm cập nhật 1 bản ghi
     * created by pdthien 21.05.2022
     * @param {*} ID 
     * @param {*} payload 
     * @returns 
     */
    model.statics.updateByID = async function (ID, payload, selectColumns = null) {
        try {
            let query = {};
            query[idName] = ID;
            let doc = await this.updateOne(query, payload);
            if(doc.modifiedCount == 0) return null;
            const result = await this.getByID(ID, selectColumns);
            return result;
        } catch (error) {
            throw error;
        }
    }
    /**
     * Hàm cập nhật list bản ghi
     * created by pdthien 21.05.2022
     * @param {*} ID 
     * @param {*} payload 
     * @returns 
     */
    model.statics.updateListByIDs = async function (ID, payload, selectColumns = null) {
        try {
            let query = {};
            query[idName] = ID;
            let doc = await this.updateMany(query, payload);
            if(doc.modifiedCount == 0) return null;
            var result = await this.find(query).select(selectColumns).lean();
            return result;
        } catch (error) {
            throw error;
        }
    }
    /**
     * Hàm xóa 1 bản ghi
     * created by pdthien 21.05.2022
     * @param {*} ID 
     * @param {*} payload 
     * @returns 
     */
    model.statics.deleteByID = async function (ID) {
        try {
            let query = {};
            query[idName] = ID;
            var result = await this.deleteOne(query);
            return result;
        } catch (error) {
            throw error;
        }
    }
    /**
     * Hàm xóa danh sách bản ghi
     * created by pdthien 21.05.2022
     * @param {*} ID 
     * @param {*} payload 
     * @returns 
     */
    model.statics.deleteListByIDs = async function (IDs) {
        try {
            let query = {};
            query[idName] = {$in: IDs};
            var result = await this.deleteMany(query);
            return result;
        } catch (error) {
            throw error;
        }
    }
};

