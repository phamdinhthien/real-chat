const mongoose = require('mongoose');

const idName = _keyModelConstant.ACCESS_KEY_MODEL;
var accessKeySchema = extendBaseSchema(idName, {
    tenantID: {
        type: String,
        unique: true
    },
    keySecret: {
        type: String,
        unique: true
    }
});

var AccessKey = mongoose.model('AccessKey', accessKeySchema);

module.exports = AccessKey;