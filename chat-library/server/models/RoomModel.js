
const mongoose = require('mongoose');

var UserModel = require('../models/UserModel');
var MessageModel = require('../models/MessageModel');
var typeMessageEnum = require('../enums/TypeMessageEnum');
var stateMessageEnum = require('../enums/StateMessageEnum');
var eventBus = require('../emitter/eventBus');

const idName = _keyModelConstant.ROOM_MODEL;
var roomSchema = extendBaseSchema(idName, {
    roomID: {
        type: Number,
        unique: true
    },
    name: {
        type: String
    },
    isGroup: Boolean,
    userIDs: {
        type: [Number],
        required: true
    },
    createdByUserID: {
        type: Number,
        required: true
    },
    createdByUserName: {
        type: String,
        required: true
    },
    lastMsgID: Number,
    lastMsgContent: String,
    lastMsgType: Number,
    lastMsgSender: Object,
    tenantID: {
        type: String
    }
});

/**
 * Hàm khởi tạo room 1-1, 1-n
 * created by pdthien 16.10.2022
 * @param {*} receiverID 
 * @param {*} isGroup 
 * @param {*} userID 
 * @param {*} teantID 
 */
roomSchema.statics.initRoom = async function(receiverID, isGroup, userID, teantID, socketID) {
    try {
    //   if(!isGroup) {
    //     let checkExistedRoom = this.findOne({userIDs: receiverID[0]});
    //     if(checkExistedRoom) return;
    //   }
      let userIDs = [userID, ...receiverID];
      let users = await UserModel.getListByIDs(userIDs);
      let sender = users.find(u => u.userID == userID);
      let senderName = sender.name;
      let roomName = isGroup ? users.map(u => u.name).join(' ,') : null;
      let payloadRoom = {
        name: roomName,
        isGroup: isGroup,
        userIDs: userIDs,
        createdByUserID: userID,
        createdByUserName: senderName,
        lastMsgID: null,
        lastMsgContent: 'init',
        lastMsgType: typeMessageEnum.INIT,
        lastMsgSender: {
          userID: userID,
          userName: senderName
        },
        tenantID: teantID
      }
      // tạo phòng chat
      let newRoom = await this.create(payloadRoom);
      let newRoomID = newRoom.roomID;
  
      // tạo message
      let payloadMsg = {
        roomID: newRoomID,
        senderID: userID,
        senderName: senderName,
        type: typeMessageEnum.INIT,
        state: stateMessageEnum.SENT,
        content: 'init',
        tenantID: teantID,
        requestID: 'init'
      }
      let newMsg = await MessageModel.create(payloadMsg);
      // update lastMsgID cho room
      await this.updateByID(newRoomID, {lastMsgID: newMsg.msgID});
      newMsg.roomInfo = newRoom;
      newMsg.usersInfo = users;
      let response = {
        senderName: newMsg.senderName,
        content: newMsg.content,
        requestID: newMsg.requestID,
        roomID: newMsg.roomID,
        senderID: newMsg.senderID,
        createdAt: newMsg.createdAt,
        msgID: newMsg.msgID,
        type: newMsg.type,
        roomInfo: newMsg.roomInfo,
        usersInfo: newMsg.usersInfo
      }
      // bắn eventBus để xử lý gọi socket
      eventBus.eventBus.emit(`eventBusSocket_${socketID}`, {
        roomID: newRoomID,
        eventName: 'initRoomEventBus',
        objectSend: response
      })
      return response;
    } catch (err) {
      throw new Error(err)
    }
  }


roomSchema.plugin(AutoIncrement, {inc_field: idName});
var Rooms = mongoose.model('Rooms', roomSchema);

module.exports = Rooms;