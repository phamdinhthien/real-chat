const mongoose = require('mongoose');

const idName = _keyModelConstant.USER_MODEL;
var userSchema = extendBaseSchema(idName, {
    userID: {
        type: Number,
        unique: true
    },
    socketIDs: Array,
    name: {
        type: String,
        required: true,
        minlength: 6,
        maxlength: 25
    },
    alias: String,
    avatar: String,
    mail: {
        type: String,
        unique: true,
        minlength: 10,
        maxlength: 50
    },
    password: {
        type: String,
        required: true,
        minlength: 8
    },
    tenantID: {
        type: String
    },
    isOnline: {
        type: Boolean,
        default: false
    }
});

userSchema.plugin(AutoIncrement, {inc_field: idName});
var Users = mongoose.model('Users', userSchema);

module.exports = Users;