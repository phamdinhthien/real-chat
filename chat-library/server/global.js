
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModel = require('./models/BaseModel');
global.AutoIncrement = require('mongoose-sequence')(mongoose);
global.extendBaseSchema = function(IDName, definition) {
    let res = new Schema({
      ...definition},
      {timestamps: true }
    );
    BaseModel(res, IDName);
    return res;
}

global.$responseAPI = require('./helpers/responseAPI');
global._stateAPIEnum = require('./enums/StateAPIEnum');
global._keyModelConstant = require('./constants/KeyModelConstant');