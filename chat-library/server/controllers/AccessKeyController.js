const BaseController = require('./BaseController');
var AccessKeyModel = require('../models/AccessKeyModel');
const { v4: uuidv4 } = require('uuid');

class AccessKeyController extends BaseController {
    constructor() {
        super(AccessKeyModel, _keyModelConstant.ACCESS_KEY_MODEL);
    }

    genAccessKey = async(req, res) => {
        try {
            const tenantID = uuidv4();
            const isExist = await AccessKeyModel.findOne({
                tenantID: tenantID
            }).lean();
            if(isExist) {
                $responseAPI.onError(res, 'User exist', 400, 'USER_EXIST');
                return;
            }
            const keySecret = uuidv4();
            const data  = await AccessKeyModel.createOne({ tenantID, keySecret });
            $responseAPI.onSuccess(res, data);
        } catch (error) {
            $responseAPI.onError(res, error);
        }
    }
}

module.exports = AccessKeyController;