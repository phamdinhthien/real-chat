const BaseController = require('./BaseController');
var Users = require('../models/UserModel');
const bcrypt = require('bcrypt');

class UserController extends BaseController {
    constructor() {
        super(Users, _keyModelConstant.USER_MODEL);
    }
    /**
     * Xử lý pasword trước khi save
     * created by pdthien
     * @param {*} req 
     * @param {*} res 
     */
    beforeSave = async (req, res) => {
        let payload = req.body;
        const salt = await bcrypt.genSalt(10);
        const hashed = await bcrypt.hash(String(payload.password), salt);
        payload.password = hashed;
    }
}

module.exports = UserController;