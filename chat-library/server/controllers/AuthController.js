
const jwt = require('jsonwebtoken');
const tokenHelper = require('../helpers/auth');
const User = require('../models/UserModel.js');
const bcrypt = require('bcrypt');

class AuthController {
    /**
     * API login
     * created pdthien 19.04.2022
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    login = async (req, res, next) => {
        try {
            let payload = req.body;
            const user = await User.findOne({
                mail: payload.mail
            }).lean();
            if(!user) {
                $responseAPI.onError(res, 'Wrong user', 404);
                return;
            }
            const validatePass = await bcrypt.compare(String(payload.password), String(user.password));
            if(!validatePass) {
                $responseAPI.onError(res, 'Wrong password', 404);
                return;
            }
            if(user && validatePass) {
                const token = await tokenHelper.createToken({userID: user.userID, name: user.name, mail: user.mail, tenantID: user.tenantID});
                res.cookie('refreshToken', token.refreshToken, {
                    httpOnly: true,
                    sameSite: true,
                    secure: false
                });
                $responseAPI.onSuccess(res, token.accessToken);
            }
        } catch (error) {
            $responseAPI.onError(res, error);
        }
    }
    /**
     * API refresh jwt
     * created pdthien 19.04.2022
     * @param {*} req 
     * @param {*} res 
     */
    refreshToken = (req, res) => {
        let refreshToken = req?.cookies?.refreshToken;
        if(!refreshToken) res.status(401);
        jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, data) => {
            if(err) {
                res.status(403);
                return $responseAPI.onError(res, err, 403);
            }
            const token = tokenHelper.createToken({userID: data.userID, name: data.name, mail: data.mail, tenantID: data.tenantID});
            res.cookie('refreshToken', token.refreshToken, {
                httpOnly: true,
                sameSite: true,
                secure: false
            });
            return $responseAPI.onSuccess(res, token.accessToken);
        })
    }

    /**
     * Hàm đăng ký user
     * created by pdthien 01.05.2022
     * @param {*} req 
     * @param {*} res 
     */
    registerUser = async(req, res) => {
        try {
            let payload = req.body;
            const salt = await bcrypt.genSalt(10);
            const hashed = await bcrypt.hash(String(payload.password), salt);
            payload.password = hashed;
            payload.tenantID = req.tenantID;
            let dataCheckExist = await User.findOne({
                mail: payload.mail
            }).lean();
            if(dataCheckExist) {
                $responseAPI.onError(res, 'User exist', 400, 'USER_EXIST');
                return;
            }
            let { mail, name }  = await User.create(new User(payload));
            $responseAPI.onSuccess(res, { mail, name });
        } catch (error) {
            $responseAPI.onError(res, error);
        }
    }
}
module.exports = AuthController;