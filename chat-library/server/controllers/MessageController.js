const BaseController = require('./BaseController');
const RoomController = require('./RoomController');
var MessageModel = require('../models/MessageModel');
var RoomModel = require('../models/RoomModel');
var eventBus = require('../emitter/eventBus');
const {decryptData} = require('../helpers/encryption');

class MessageController extends BaseController {
    constructor() {
        super(MessageModel, _keyModelConstant.MESSAGE_MODEL);
    }
    customParam = async(req, queryPaging) => {
        let userID = req.userID;
        queryPaging['$and'].push({isVisibleToGroup: true}, {deletedBy: {$nin: [userID]}});
    }
    /**
     * Kiểm tra người truy cập có quyền truy cập room
     * - Lấy paging theo permission (userID + roomID + tenantID)
     * created by pdthien 09.10.2022
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    validateGetPaging = async (req, res) => {
        let payload = req.body;
        let userID = req.userID;
        let tenantID = req.tenantID;
        let roomID = payload.roomID;
        let room = await RoomModel.findOne({roomID, tenantID}).lean();
        if(!room) return {code: 'NOT_PERMITTED'};
        let participants = room.userIDs;
        if(!participants.includes(userID)) {
            return {code: 'NOT_PERMITTED'};
        }
        return undefined;
    }
    modifyDoc(doc) {
        doc.forEach(e => {
            e.content = decryptData(e.content);
        })
        return doc;
    }
    /**
     * Xử lý lấy tin nhắn khi người dùng chọn người nhắn tin cụ thể
     * - Nếu đã có room 1-1 trước đó => trả về kết quả msg luôn
     * - Nếu chưa tồn tạo room 1-1 => khởi tạo room 1-1
     * @param {*} req 
     * @param {*} res 
     */
    getMsgByUser = async (req, res) => {
        let receiverID = req.body.receiverID;
        let checkExistedRoom = await RoomModel.findOne({userIDs: receiverID, isGroup:false}).lean();
        if(checkExistedRoom) {
            let roomID = checkExistedRoom.roomID;
            req.body = {
                pageIndex: 1,
                pageSize: 1,
                filter: `[["roomID","=",${roomID}]]`,
                sort: {
                    updatedAt: -1
                }
            }
            await (new RoomController()).getPaging(req, res);
        } else {
            let result = await RoomModel.initRoom([receiverID], false, req.userID, req.tenantID, req.body.socketID);
            $responseAPI.onSuccess(res, result);
        }
    }

    validateUpdateField = async(req, res) => {
        return this.validateGetPaging(req, res);
    }
    /**
     * Hàm xoá tin nhắn
     * created by pdthien 23.05.2022
     * @param {*} req 
     * @param {*} res 
     */
    deleteMsg = async (req, res) => {
        let userID = req.userID;
        let payload = req.body;
        let msgID = payload.msgID;
        let msg = await this.Model.getByID(msgID);
        let deletedBy = msg.deletedBy;
        if(!deletedBy.includes(userID)) {
            deletedBy.push(userID);
            req.body = {
                msgID: msgID,
                data: {
                    deletedBy: deletedBy
                },
                roomID: payload.roomID
            }
            await this.updateField(req, res);
        }
    }

    /**
     * Hàm thu hồi tin nhắn
     * created by pdthien 23.05.2022
     * @param {*} req 
     * @param {*} res 
     */
    undoMsg = async (req, res) => {
        let userID = req.userID;
        let payload = req.body;
        let msgID = payload.msgID;
        let msg = await this.Model.getByID(msgID);
        if(msg.senderID != userID) return $responseAPI.onError(res, null, 403, 'NOT_PERMITTED');
        let socketID = req.body.socketID;
        req.body = {
            msgID: msgID,
            data: {
                isVisibleToGroup: false
            },
            roomID: payload.roomID
        }
        await this.updateField(req, res);
        // bắn eventBus để xử lý gọi socket
        eventBus.eventBus.emit(`eventBusSocket_${socketID}`, {
            roomID: payload.roomID,
            eventName: 'undoMsg',
            objectSend: {
                msgID: msgID
            }
        })
    }
}

module.exports = MessageController;