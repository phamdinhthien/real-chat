const BaseController = require('./BaseController');
var RoomModel = require('../models/RoomModel');
var UserModel = require('../models/UserModel');
const {decryptData} = require('../helpers/encryption');

class RoomController extends BaseController {
    constructor() {
        super(RoomModel, _keyModelConstant.ROOM_MODEL);
    }

    /**
     * Hàm custom paging
     * - Lấy paging những room theo permission (userID + tenantID)
     * created by pdthien
     * @param {*} req 
     * @param {*} pageIndex 
     * @param {*} pageSize 
     * @param {*} sortPaging 
     * @param {*} queryPaging 
     * @returns 
     */
    customGetPaging = async (req, pageIndex, pageSize, sortPaging, queryPaging) => {
        sortPaging = sortPaging || {updatedAt : -1}
        queryPaging['$and'].push({userIDs: req.userID})
        let doc = await this.Model.aggregate([
            { $match: queryPaging },
            { $unwind: "$userIDs" },
            {
                $lookup: {
                    from: 'users',
                    localField: 'userIDs',
                    foreignField: 'userID',
                    as: 'users',
                }
            },
            { $unwind: "$users" },
            { $group: {
                "_id": "$_id",
                "users": { "$push": {id: "$users.userID", name: "$users.name", isOnline: "$users.isOnline"} },
                "name": { $last: '$name' },
                "isGroup": { $last: '$isGroup' },
                "createdByUserID": { $last: '$createdByUserID' },
                "createdByUserName": { $last: '$createdByUserName' },
                "lastMsgID": { $last: '$lastMsgID' },
                "lastMsgContent": { $last: '$lastMsgContent' },
                "lastMsgType": { $last: '$lastMsgType' },
                "lastMsgSender": { $last: '$lastMsgSender' },
                "createdAt": { $last: '$createdAt' },
                "updatedAt": { $last: '$updatedAt' },
                "roomID": { $last: '$roomID' },
            }},
            { $skip: (pageSize * pageIndex) - pageSize },
            { $limit: pageSize },
            { $sort: sortPaging }
        ])
        doc
        return doc;
    }
    modifyDoc(doc) {
        doc.forEach(e => {
            e.lastMsgContent = decryptData(e.lastMsgContent);
        })
        return doc;
    }
    /**
     * Xử lý tạo room 1-n
     * create by pdthien 16/10/2022
     * @param {*} req 
     * @param {*} res 
     */
    createRoomGroup = async (req, res) => {
        let receiverIDs = req.body.receiverIDs;
        let data = await this.Model.initRoom(receiverIDs, true, req.userID, req.tenantID, req.body.socketID);
        $responseAPI.onSuccess(res, data);
    }

    /**
     * Hàm xử lý xoá member khởi room
     * create by pdthien 23.10.2022
     * @param {*} req 
     * @param {*} res 
     */
    deleleUser = async (req, res) => {
        let payload = req.body;
        let room = await this.Model.getByID(payload.roomID, 'userIDs');
        room.userIDs.splice(room.userIDs.findIndex(u => u == payload.userID), 1);
        req.body = {
            roomID: payload.roomID,
            data: {
                userIDs: room.userIDs
            }
        }
        await this.updateField(req, res);
    }

    /**
     * Hàm xử lý xoá member khởi room
     * create by pdthien 23.10.2022
     * @param {*} req 
     * @param {*} res 
     */
     addUser = async (req, res) => {
        try {
            let validate = await this.validateUpdateField(req, res);
            if(validate) {
                if(validate.code == 'NOT_PERMITTED') {
                    return $responseAPI.onError(res, null, 403, 'NOT_PERMITTED');
                }
            }
            let result = {
                data: null,
                number: 0
            }
            let payload = req.body;
            let room = await this.Model.getByID(payload.roomID, 'userIDs');
            let noneExistedUser = payload.userIDs.filter(userID => !room.userIDs.includes(userID));
            if(noneExistedUser.length > 0) {
                room.userIDs.push(...noneExistedUser);
                await this.Model.updateByID(payload.roomID, {
                    userIDs: room.userIDs
                });
                result.number = noneExistedUser.length;
                let users = await UserModel.getListByIDs(noneExistedUser, {
                    userID: true,
                    name: true,
                    isOnline: true
                });
                result.data = users.map(u => {
                    return {
                        id: u.userID,
                        name: u.name,
                        isOnline: u.isOnline
                    }
                })
            }
            $responseAPI.onSuccess(res, result);
        } catch (error) {
            $responseAPI.onError(res, error);
        }
    }

    validateUpdateField = async(req, res) => {
        let payload = req.body;
        let roomID = payload.roomID;
        let userID = req.userID;
        let tenantID = req.tenantID;
        let room = await RoomModel.findOne({roomID, tenantID}).lean();
        if(!room) return {code: 'NOT_PERMITTED'};
        if(room.createdByUserID != userID) {
            return {code: 'NOT_PERMITTED'};
        }
    }
}

module.exports = RoomController;