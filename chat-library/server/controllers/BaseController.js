class BaseController {
    constructor(model, id) {
        this.Model = model;
        this.ID = id;
    }
    /**
     * Hàm validate trước save
     * created by pdthien 22.04.2022
     */
    validateSave = (req, res) => {}
    /**
     * Hàm xử lý trước save
     * created by pdthien 22.04.2022
     */
    beforeSave = (req, res) => { }
    /**
     * Hàm xử lý sau save
     * created by pdthien 22.04.2022
     */
    afterSave = (req, res, result) => {}
    /**
     * Hàm lưu một bản ghi
     * created by pdthien 19.04.2022
     * @param {*} req 
     * @param {*} res 
     */
    save = async(req, res) => {
        try {
            let tenantID = req.tenantID;
            this.validateSave(req, res);
            await this.beforeSave(req, res);
            let result = {data: [], number: 0};
            let payload = req.body;
            payload["tenantID"] = tenantID;
            let state = payload.state; // 1: add, 2: update, 3: delete
            let ID = payload[this.ID];
            // Thêm bản ghi
            if(state == _stateAPIEnum.ADD) {
                let data  = await this.Model.create(payload);
                result.data = await this.Model.getByID(data[this.ID], this.genSelectColumns());
                result.number = 1;
            } 
            // Cập nhật bản ghi
            else if(state == _stateAPIEnum.UPDATE) {
                result.data = await this.Model.updateByID(ID, {...payload}, this.genSelectColumns());
                result.number = result.data ? 1 : 0;
            } 
            // Xóa bản ghi
            else if(state == _stateAPIEnum.DELETE) {
                let doc = await this.Model.deleteByID(ID);
                if(doc.deletedCount > 0) result.data = true;
                else result.data = false;
                result.number = doc.deletedCount;
            }
            this.afterSave(req, res, result);
            $responseAPI.onSuccess(res, result);
        } catch (error) {
            $responseAPI.onError(res, error);
        }
    }
    /**
     * Hàm validate trước save
     * created by pdthien 22.04.2022
     */
     validateSaveList = (req, res) => {}
     /**
      * Hàm xử lý trước save
      * created by pdthien 22.04.2022
      */
     beforeSaveList = (req, res) => {}
     /**
      * Hàm xử lý sau save
      * created by pdthien 22.04.2022
      */
     afterSaveList = (req, res, result) => {}
    /**
     * Hàm lưu nhiều bản ghi
     * created by pdthien 20.04.2022
     * @param {*} req 
     * @param {*} res 
     */
    saveList = async(req, res) => {
        try {
            let tenantID = req.tenantID;
            this.validateSaveList(req, res);
            this.beforeSaveList(req, res);
            let result = {data: [], number: {insert: 0, update: 0, delete: 0}};
            let payload = req.body;
            let insertRecords = payload.filter(e => e.state == _stateAPIEnum.ADD);
            let updateRecords = payload.filter(e => e.state == _stateAPIEnum.UPDATE);
            let deleteRecords = payload.filter(e => e.state == _stateAPIEnum.DELETE);
            // Thêm bản ghi
            if(insertRecords.length > 0) {
                let data = [];
                for(let i = 0; i < insertRecords.length; i++) {
                    let record = insertRecords[i];
                    record.tenantID = tenantID;
                    let resCreated  = await this.Model.create(record);
                    let resFind = await this.Model.getByID(resCreated[this.ID], this.genSelectColumns());
                    data.push(resFind);
                }
                result.data.push(...data);
                result.number.insert = data.length;
            } 
            // Cập nhật bản ghi
            if(updateRecords.length > 0) {
                let lstUpdatedData = [];
                for(let i = 0; i < updateRecords.length; i++) {
                    let record = updateRecords[i];
                    let ID = record[this.ID];
                    let data = await this.Model.updateByID(ID, {...record}, this.genSelectColumns());
                    if(data) {
                        lstUpdatedData.push(data)
                    }
                }
                result.data.push(...lstUpdatedData);
                result.number.update = lstUpdatedData.length;
            }
            // Xóa bản ghi
            if(deleteRecords.length > 0) {
                let IDs = deleteRecords.map(e => e[this.ID]);
                let doc = await this.Model.deleteListByIDs(IDs);
                if(doc.deletedCount > 0) result.data = true;
                else result.data = false;
                result.number.delete = doc.deletedCount;
            }
            this.afterSaveList(req, res, result);
            $responseAPI.onSuccess(res, result);
        } catch (error) {
            $responseAPI.onError(res, error);
        }
    }
    /**
     * Hàm validate trước delete
     * created by pdthien 22.04.2022
     */
     validateDeleteByID = (req, res) => {}
     /**
      * Hàm xử lý trước delete
      * created by pdthien 22.04.2022
      */
     beforeDeleteByID = (req, res) => {}
     /**
      * Hàm xử lý sau delete
      * created by pdthien 22.04.2022
      */
     afterDeleteByID = (req, res, result) => {}
    /**
     * Hàm xóa bản ghi theo ID
     * created by pdthien 21.04.2022
     * @param {*} req 
     * @param {*} res 
     */
    deleteByID = async(req, res) => {
        try {
            this.validateDeleteByID(req, res);
            this.beforeDeleteByID(req, res);
            let payload = req.body;
            let doc = await this.Model.deleteByID(payload[this.ID]);
            if(doc.deletedCount > 0) result.data = true;
            else result.data = false;
            result.number = doc.deletedCount;
            this.afterDeleteByID(req, res, result);
            $responseAPI.onSuccess(res, result);
        } catch (error) {
            $responseAPI.onError(res, error);
        }
    }
    /**
     * Hàm validate trước delete
     * created by pdthien 22.04.2022
     */
     validateDeleteByListID = (req, res) => {}
     /**
      * Hàm xử lý trước delete
      * created by pdthien 22.04.2022
      */
     beforeDeleteByListID = (req, res) => {}
     /**
      * Hàm xử lý sau delete
      * created by pdthien 22.04.2022
      */
     afterDeleteByListID = (req, res, result) => {}
    /**
     * Hàm xóa bản ghi theo list ID
     * created by pdthien 21.04.2022
     * @param {*} req 
     * @param {*} res 
     */
    deleteByListID = async(req, res) => {
        try {
            this.validateDeleteByListID(req, res);
            this.beforeDeleteByListID(req, res);
            let payload = req.body;
            let doc = await this.Model.deleteListByIDs(payload[this.ID]);
            if(doc.deletedCount > 0) result.data = true;
            else result.data = false;
            result.number = doc.deletedCount;
            this.afterDeleteByListID(req, res, result);
            $responseAPI.onSuccess(res, result);
        } catch (error) {
            $responseAPI.onError(res, error);
        }
    }
    /**
     * Hàm validate trước update
     * created by pdthien 22.04.2022
     */
     validateUpdateField = (req, res) => {}
     /**
      * Hàm xử lý trước update
      * created by pdthien 22.04.2022
      */
     beforeUpdateField = (req, res) => {}
     /**
      * Hàm xử lý sau update
      * created by pdthien 22.04.2022
      */
     afterUpdateField = (req, res, result) => {}
    /**
     * Cập nhật field của record
     * created by pdthien 20.04.2022
     * @param {*} req 
     * @param {*} res 
     */
    updateField = async(req, res) => {
        try {
            let validate = await this.validateUpdateField(req, res);
            if(validate) {
                if(validate.code == 'NOT_PERMITTED') {
                    return $responseAPI.onError(res, null, 403, 'NOT_PERMITTED');
                }
            }
            this.beforeUpdateField(req, res);
            let result = {data: [], number: 0};
            let payload = req.body;
            let dataUpdate = payload.data;
            let data = await this.Model.updateListByIDs(payload[this.ID], {...dataUpdate}, this.genSelectColumns());
            result.data = data
            result.number = data ? data.length : 0;
            this.afterUpdateField(req, res, result);
            $responseAPI.onSuccess(res, result);
        } catch (error) {
            $responseAPI.onError(res, error);
        }
    }

     /**
     * Hàm validate trước update
     * created by pdthien 22.04.2022
     */
    validateGetPaging = (req, res) => {}
    /**
     * Hàm xử lý trước update
     * created by pdthien 22.04.2022
     */
    beforeGetPaging = (req, res) => {}
    /**
     * Hàm xử lý sau update
     * created by pdthien 22.04.2022
     */
    afterGetPaging = (req, res, result) => {}
    customGetPaging = null;
    customParam = null;
    /**
     * Hàm paging
     * created by pdthien 19.04.2022
     * @param {*} req 
     * @param {*} res 
     */
    getPaging = async(req, res) => {
        try {
            let tenantID = req.tenantID;
            let validate = await this.validateGetPaging(req, res);
            if(validate) {
                if(validate.code == 'NOT_PERMITTED') {
                    return $responseAPI.onError(res, null, 403, 'NOT_PERMITTED');
                }
            }
            this.beforeGetPaging(req, res);
            let result = {data: [], number: 0};
            let payload = req.body;
            let pageIndex = payload.pageIndex || 1;
            let pageSize = payload.pageSize || 25;
            let sortPaging = payload.sort || {};
            let selectColumns = this.genSelectColumns(payload);
            let queryPaging = this.genQueryPaging(payload, tenantID);
            if(this.customParam) {
                this.customParam(req, queryPaging);
            }
            let doc = null;
            if(!this.customGetPaging) {
                let aggregateParam = []
                if(Object.keys(sortPaging).length > 0) aggregateParam.push({ $sort: sortPaging });
                aggregateParam.push(
                    { $match: queryPaging },
                    { $project: selectColumns },
                    { $skip: (pageSize * pageIndex) - pageSize },
                    { $limit: pageSize }
                );
                doc = await this.Model.aggregate(aggregateParam);
            } else {
                doc = await this.customGetPaging(req, pageIndex, pageSize, sortPaging, queryPaging);                
            }
            this.modifyDoc(doc);
            result.data = doc;
            result.total = doc.length;
            this.afterGetPaging(req, res, result);
            $responseAPI.onSuccess(res, result);
        } catch (error) {
            $responseAPI.onError(res, error);
        }
    }
    modifyDoc(doc) {
        return doc;
    }
    toLowerCaseNonAccentVietnamese(str) {
        if(!str) return "";
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        return str;
    }

    /**
     * Hàm generate search query
     * created by pdthien 19.04.2022
     * @param {*} search 
     * @returns 
     */
    genSearchQuery(search) {
        let querySearch = [];
        let columns = search.columns;
        let valueSearch = this.toLowerCaseNonAccentVietnamese(search.value);
        columns && columns.forEach(e => {
            let objSearch = {};
            objSearch[e] = new RegExp(valueSearch, 'gi');
            querySearch.push(objSearch);
        });
        return querySearch;
    }
    /**
     * Hàm gen filter
     * created by pdthien 22.04.2022
     * @param {*} filter 
     * @returns 
     */
    genFilterQuery(filter, tenantID) {
        let result = {};
        let queryFilter = [];
        let innitCondition = {tenantID: tenantID};
        // filter = '[["name","<>","abc"],"AND",["code","=","123"]]'
        if(!this.checkEmptyObject(filter)) {
            let filterArr = JSON.parse(filter);
            let filterArrEven = filterArr.filter((e, index) => index%2 == 0);
            let filterArrOdd = filterArr.filter((e, index) => index%2 != 0);
            filterArrEven.forEach((e) => {
                let filterObj = {};
                let fieldName = e[0];
                let fieldOperator = e[1];
                let fieldValue = e[2];
                filterObj[fieldName] = this.genFilterOperatorValue(fieldOperator, fieldValue);
                queryFilter.push(filterObj);
            })
            if(filterArrOdd.length == 0) {
                queryFilter.push(innitCondition);
                return queryFilter;
            }
            result[this.genFilterOperatorLink(filterArrOdd[0])] = queryFilter;
            if(result["$and"]) {
                result["$and"].push(innitCondition);
            } else {
                result["$and"] = innitCondition;
            }
        } else {
            result = innitCondition;
        }
        return [result];
    }
    /**
     * Hàm gen liên kết điều kiện filter
     * created by pdthien 23.04.2022
     * @param {*} link 
     * @returns 
     */
    genFilterOperatorLink(link) {
        let res = null;
        switch (link.toUpperCase()) {
            case "AND":
                res = "$and";
                break;
            case "OR":
                res = "$or";
                break;
            default:
                break;
        }
        return res;
    }
    /**
     * Hàm gen toán tử filter
     * created by pdthien 23.04.2022
     * @param {*} fieldOperator 
     * @returns 
     */
    genFilterOperator(fieldOperator) {
        let opertor = null;
        switch (fieldOperator) {
            case "=":
                opertor = "$eq";
                break;
            case "<>":
                opertor = "$ne";
                break;
            default:
                break;
        }
        return opertor;
    }
    /**
     * Hàm gen filter
     * @param {*} fieldOperator 
     * @param {*} fieldValue 
     * @returns 
     */
    genFilterOperatorValue(fieldOperator, fieldValue) {
        let filterOperator = {};
        let opertor = this.genFilterOperator(fieldOperator);
        filterOperator[opertor] = fieldValue;
        return filterOperator;
    }
    /**
     * Hàm gen các cột trả về
     * created by pdthien 22.04.2022
     * @param {*} payload 
     * @returns 
     */
    genSelectColumns(payload) {
        let result = {_id: 0, __v: 0};
        if(!payload) return result;
        let columns =  payload.columns;
        if(columns) {
            let columnArr = columns.replace(/\s/g, '').split(',');
            columnArr.forEach(c => {
                result[c] = 1;
            })
        }
        return result;
    }
    /**
     * Hàm tạo query paging
     * created by pdthien 21.04.2022
     * @param {*} payload 
     * @returns 
     */
    genQueryPaging(payload, tenantID) {
        let search = payload.search || {};
        let filter = payload.filter || {};
        let querySearch = this.genSearchQuery(search);
        let queryFilter = this.genFilterQuery(filter, tenantID);
        let queryPaging = {};
        if(querySearch.length > 0) {
            queryPaging['$or'] = querySearch;
        }
        if(queryFilter.length > 0) {
            queryPaging['$and'] = queryFilter;
        }
        return queryPaging;
    }
    /**
     * Hàm check obj trống
     * created by pdthien 22.04.2022
     * @param {*} obj 
     * @returns 
     */
    checkEmptyObject(obj) {
        for(var i in obj) return false;
        return true;
    }
};

module.exports = BaseController;