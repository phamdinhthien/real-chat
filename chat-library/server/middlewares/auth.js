const jwt = require('jsonwebtoken');
var AccessKeyModel = require('../models/AccessKeyModel');

function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(jsonPayload);
}
module.exports = {
    /**
     * Hàm verity jwt
     * created by pdthien 19.04.2022
     * @param {*} req 
     * @param {*} res 
     */
    verifyAccessToken: async(req, res, next) => {
        let authorizationHeader = req.headers['authorization'];
        if(!authorizationHeader) {
            $responseAPI.onError(res, 'UNAUTHEN', 401);
            return;
        }
        let token = authorizationHeader.split(' ')[1];
        if(!token) {
            $responseAPI.onError(res, 'UNAUTHEN', 401);
            return;
        }
        try {
            let parseJwtObject = parseJwt(token);
            let tenantID = parseJwtObject.tenantID;
            let accessKey = await AccessKeyModel.findOne({tenantID});
            jwt.verify(token, accessKey.keySecret, function(err, decode) {
                if(err) {
                    if(err.name === "TokenExpiredError")
                    return $responseAPI.onError(res, err, 401);
                }
                let tenantID = decode.tenantID;
                let userID = decode.userID;
                req.tenantID = tenantID;
                req.userID = userID;
                next();
            });
          } catch(err) {
            $responseAPI.onError(res, err, 403);
            return;
          }
    },
    /**
     * Authen cho socket
     * created by pdthien 06.06.2022
     * @param {*} token 
     * @returns 
     */
    verifyAccessTokenForSocket: async(token) => {
        if(!token) return {
            code: 'UNAUTHEN',
            status: 401
        };
        try {
            let parseJwtObject = parseJwt(token);
            let tenantID = parseJwtObject.tenantID;
            let accessKey = await AccessKeyModel.findOne({tenantID});
            let res = await jwt.verify(token, accessKey.keySecret);
            if(res) return {
                status: 200
            };
            return {
                code: 'UNAUTHEN',
                status: 401
            };
        } catch (err) {
            if(err.name === "TokenExpiredError") return {
                code: 'TokenExpiredError',
                status: 401
            };
            
            return {
                code: 'UNAUTHEN',
                status: 401
            };
        }
    }
}