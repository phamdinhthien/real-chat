const path = require('path');

module.exports = {
    mode: "none",
    entry: './client/chat-library-main.js',
    output: {
        filename: './server/public/chat-library.js',
        path: path.resolve(__dirname)
    }
}