
// import thư viện
<script src="https://cdn.jsdelivr.net/gh/phamdinhthien/realtime-chat-cdn@main/real-chat.js"></script>

// khởi tạo đối tượng RealClient
var realClient = new RealClient();

// Gọi API login để khởi tạo token gửi trả về client
const axios = require('axios').default;
var instance = axios.create({
    baseURL: 'http://localhost/api/v1/',
    headers: { 'Content-Type': 'application/json' }
});
instance.post('auth/login', payload).then(res => {
    let token = res.data.result;
    realClient.connect(token);
});

//................................................................

// Lắng nghe thành viên trong nhóm gõ tin nhắn
realClient.on('userBeginTyping', (res) => {
    // to do
});

// Lắng nghe thành viên trong nhóm dừng gõ tin nhắn
realClient.on('userEndTyping', (res) => {
    // to do
});

//................................................................

// Lắng nghe cuộc gọi tới
realClient.on('receiveCall', (res) => {
    // to do
});

// Lắng nghe người nhận đồng ý cuộc gọi
realClient.on('receiverAgreeAnswer', (res) => {
    // to do
});

// Lắng nghe người nhận từ chối cuộc gọi
realClient.on('receiverCancelAnswer', (res) => {
    // to do
});

//................................................................

// Lắng nghe trạng thái online/offline
realClient.on('triggerUserStatus', (res) => {
    // to do
});

//................................................................

// Gửi tin nhắn
realClient.sendMessage(data, 
    // Hàm xử lý trước khi gửi tin nhắn
    (payload) => {
        // to do
    },
    // Hàm xử lý lắng nghe trạng thái tin nhắn theo thời gian thực
    (result) => {
        // to do
    }
);

// Gửi tin nhắn tệp (file, audio, video, ...)
realClient.uploadFile(file).then((res) => {
    // Gửi tin nhắn
    realClient.sendMessage();
});

// Lắng nghe tin nhắn tới
realClient.on('receiveMessage', (res) => {
    // to do
});

//................................................................

// Bắt đầu cuộc gọi (audio hoặc video)
realClient.startCall(roomID, isCallVideo);

// Chấp nhận cuộc gọi (audio hoặc video)
realClient.aggreeAnswer(roomID, isCallVideo);

// Tắt cuộc gọi
realClient.cancelAnswer(roomID);

// Chia sẻ màn hình
realClient.shareScreen(roomID);

// bật/tắt audio
realClient.toggleMuteAudio();

// bật/tắt video
realClient.toggleMuteVideo();

//................................................................

// Tạo nhóm chat
realClient.createRoomGroup(receiverIDs, (res) => {
    // to do
});

// Thêm thành viên vào nhóm chat
realClient.addUserInRoom(roomID, receiverIDs, (res) => {
    // to do
});

// Xóa thành viên khỏi nhóm chat
realClient.deleteUserInRoom(roomID, userID, (res) => {
    // to do
});

// Đổi tên nhóm chat
realClient.editRoomName(roomID, newName);

// Đánh dấu nhóm chat đã đọc
realClient.markReadMessage(currentRoomID);

// Lắng nghe tin nhắn tới
realClient.on('receiveMessage', (res) => {
    // to do
});

//................................................................

// Xóa tin nhắn
realClient.deleteMsg(roomID, msgID, (res) => {
    // to do
});

// Thu hồi tin nhắn
realClient.undoMsg(roomID, msgID, (res) => {
    // to do
});

// Lắng nghe người gửi thu hồi tin nhắn
realClient.on('senderUndoMsg', (res) => {
    // to do
});
