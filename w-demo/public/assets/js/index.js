const msgerInput = get(".msger-input");
const msgerChat = get(".msger-chat");
const msgeRoom = get(".list-room");
const msgeRoomSearch = get(".list-room-search");
const leftsideElm = get(".left-side");
const msgeCallPopup = get(".msger-popup-call-root");
const msgeWaitCallPopup = get(".msger-popup-wait-call-root");
const msgUserTyping = get(".user-typing");
const msgSearchInput = get('.msger-seach');
const listUsersPopup = get('.list-users-popup');
const listUsersElm = get('#lst-user');
const roomInfoElm = get('#room-info');
const dropdownElm = get('.dropdown-root');

var disabledCheckScrollEvent = false;
var USER_INFO = {};
window.currentRoomID = null;
var dateArray = [];
var pageIndexMsg = 1;
var userTyping = {};
var typingTimeout = null;
var allRoom = [];
var currentRoomInfo = null;
var isCallVideo = false;
var intervalTimeCall = null;
var filesInChat = [];
var typeMsgENUM = {
    INIT: 1,
    TEXT: 2,
    AUDIO: 3,
    VIDEO: 4,
    PHOTO: 5,
    FILE: 6,
    LINK: 7,
    STICKER: 8,
    CONTACT: 9,
    LOCATION: 10,
    RENAME_ROOM: 11,
}

var realClient = new RealClient();
var accessToken = null;
accessToken = localStorage.getItem('real_chat_access_token');
if(!accessToken) location.replace('/login.html');
realClient.connect(accessToken);

realClient.on('unauthen', (res) => {
    location.replace('/login.html');
});

realClient.on('connected', (res) => {
    console.log("client: " + res);
    USER_INFO = res;
    handleCallAPIRoom({sort: {updatedAt: -1}}, (res) => {
        if(!res.success) return;
        allRoom = res.result.data
        currentRoomInfo = allRoom[0];
        currentRoomID = currentRoomInfo.roomID;
        realClient.markReadMessage(currentRoomID);
        renderRooms(res.result.data);
        getAndRenderMsgs(pageIndexMsg);
    })
})

realClient.on('userBeginTyping', (res) => {
    if(!checkCurrentRoomToRender(res.roomID)) return;
    handleUserBeginTyping(res);
})

realClient.on('userEndTyping', (res) => {
    if(!checkCurrentRoomToRender(res.roomID)) return;
    handleUserEndTyping(res);
})

realClient.on('receiveMessage', (res) => {
    updateInfoRoom(res);
    if(!checkCurrentRoomToRender(res.roomID)) return;
    let position = "left";
    if(res.senderID == USER_INFO.userID) position = "right";
    appendMessage(res.senderName, 'https://image.flaticon.com/icons/svg/145/145867.svg', position, res, false);
})
realClient.on('receiveCall', (res) => {
    isCallVideo = res.isCallVideo;
    msgeCallPopup.classList.add('d-block');
})
realClient.on('receiverAgreeAnswer', (res) => {
    msgeWaitCallPopup.classList.remove("d-block");
    if(!res.isCallVideo) {
        document.getElementById('call-audio').classList.add('d-block');
    } else {
        document.getElementById('video-grid').classList.add('d-flex');
    }
    countCallTime();
})

realClient.on('receiverCancelAnswer', (res) => {
    msgeWaitCallPopup.querySelector('.content').innerHTML = "Từ chối cuộc gọi";
    document.getElementById('call-audio').classList.remove('d-block');
    document.getElementById('video-grid').classList.remove('d-flex');
    document.getElementById('video-grid').querySelectorAll('video').forEach(e => e.remove())
    clearInterval(intervalTimeCall);
})

realClient.on('receiverMarkReadMsg', (res) => {
    (res.roomID == currentRoomID) && document.querySelectorAll('.msg-status').forEach(e => {
        e.querySelector('.fa').classList.add('d-none-important');
    })
})

realClient.on('triggerUserStatus', (res) => {
    allRoom.forEach(room => {
        let checkUser = room.users.find(u => u.id == res.senderID && res.senderID != USER_INFO.userID);
        if(checkUser == undefined) return;
        document.getElementById(`room-id-${room.roomID}`).querySelector('.status').innerHTML = (res.isOnline ? 'online': 'offline');
    });
})

realClient.on('receiverRenameRoom', (res) => {
})

realClient.on('senderUndoMsg', (res) => {
    get(`#msg-id-${res.msgID}`).remove();
    dropdownElm.style.display = 'none';
})

realClient.on('watchMsgStatus', (res) => {
    let msgRootElm = document.getElementById(`msg-id-${res.requestID}`) || document.getElementById(`msg-id-${res.msgID}`);
    if(msgRootElm) msgRootElm.setAttribute("id", `msg-id-${res.msgID}`);
    let elm = genIconMsgStatus(res.readBy, res.state);
    let statusElm = document.getElementById(`msg-status-${res.requestID}`) || document.getElementById(`msg-status-${res.msgID}`);
    if(!statusElm || !elm) return;
    if(msgRootElm.querySelector('.msg-file')) {
        let newHef = msgRootElm.querySelector('.msg-file').getAttribute('href').replace(/(msgID=)[^\&]+/, '$1' + res.msgID);
        msgRootElm.querySelector('.msg-file').setAttribute('href', newHef);
    }
    statusElm.setAttribute("id", `msg-status-${res.msgID}`);
    statusElm.innerHTML = elm;
    let msgItemElm = statusElm.nextElementSibling.nextElementSibling;
    msgItemElm.querySelector(".fa-ellipsis-h").onclick = () => openMoreFeatureMsg(res.msgID, res.senderID);
    msgItemElm.querySelector(".msg-bubble").setAttribute('readby', JSON.stringify(res.readBy));
    genTitleUserReadMsg(msgItemElm, res.readBy, null)
})

function get(selector, root = document) {
    return root.querySelector(selector);
}
function formatDate(date) {
    date = new Date(date);
    const h = "0" + date.getHours();
    const m = "0" + date.getMinutes();
    return `${h.slice(-2)}:${m.slice(-2)}`;
}
function formatDay(date) {
    date = new Date(date);
    let day = "0" + date.getDate();
    let month = "0" + (date.getMonth() + 1);
    let year = date.getFullYear();
    return `${day.slice(-2)}/${month.slice(-2)}/${year}`;
}

function handleCall(callVideo = false) {
    isCallVideo = callVideo;
    realClient.startCall(currentRoomID, callVideo);
    msgeWaitCallPopup.classList.add("d-block");
    document.getElementById('call-audio').querySelector('.call-audio-name').innerHTML = `<b>${currentRoomInfo.name}</b>`;
}

function handleUpload() {
    let file = document.getElementById('uploadFile').files[0];
    var data = new FormData();
    data.append('files', file)
    realClient.uploadFile(data).then((res) => {
        let data = res.result;
        data.forEach(e => {
            let payload = {
                content: e.filename,
                userID: USER_INFO.userID,
                roomID: currentRoomID,
                type: 6,
                createdAt: new Date()
            }
            realClient.sendMessage(payload, (p) => {
                payload.msgID = p.requestID;
                appendMessage(USER_INFO.name, 'https://image.flaticon.com/icons/svg/145/145867.svg', 'right', payload, false);
                let msgFilesElm = document.querySelector(".msger-files");
                msgFilesElm.innerHTML = '';
            })
        })
    })
}

function genIconMsgStatus(readBy, state) {
    if(Array.isArray(readBy) && readBy.length == 0) readBy = null;
    return `<i class="fa ${readBy ? 'd-none' : (state == 2 ? 'fa-check-circle-o' : state == 3 ? 'fa-check-circle' : 'fa-circle-thin')}"></i>`;
}

function appendMessage(name, img, side, val, returnValue = true) {
    let checkExist = true;
    if(!dateArray.includes(formatDay(val.createdAt))) {
        dateArray.unshift(formatDay(val.createdAt));
        checkExist = false;
    }
    if(!val.senderID) val.senderID = val.userID;
    let readBy  = val.readBy ? val.readBy.filter(e => e.readByUserID && e.readByUserID != USER_INFO.userID).map(e => e.readByUserID) : null;
    let contentElm = null;
    switch (val.type) {
        case typeMsgENUM.TEXT:
            contentElm = `<div class="msg-text">${val.content}</div>`
            break;
        case typeMsgENUM.FILE:
            let contentArr = val.content.split('-');
            let content = contentArr.slice(1, contentArr.length - 1).join('-');
            contentElm = `<a class="msg-file d-flex w-full" href="${'https://pdthien.store:4000/api/v1/file/get-file' + '?path=' + val.content + '&roomID=' + val.roomID + '&msgID=' + val.msgID + '&accessToken=' + accessToken}" target="_blank" file_extension="${getFileExtesion(content)}" title="${content}">
                            <div class="file-icon"></div>
                            <div class="file-name w-full">${content}</div>
                        </a>`;
            break;
        default:
            break;
    }
    var msgHTML = `
        <div class="${!checkExist ? 'date-send-msg' : 'd-none'}">${formatDay(val.createdAt)}</div>
        <div class="msg ${side}-msg" id="msg-id-${val.msgID}">
            <div class="msg-status ${val.senderID != USER_INFO.userID ? 'd-none' : ''}" id="msg-status-${val.msgID}">
                ${genIconMsgStatus(readBy, val.state)}
            </div>
            
            <div class="msg-img ${val.senderID == USER_INFO.userID ? 'd-none' : ''}" style="background-image: url(${img})"></div>
            <div class="d-flex msg-bubble-root">
                <i class="fa fa-ellipsis-h" onclick="openMoreFeatureMsg(${val.msgID}, ${val.senderID})" ></i>
                <div class="msg-bubble" onmouseover="handleHoverMsg(this, ${val.senderID})" readby='${JSON.stringify(val.readBy)}'>
                    <div class="msg-info">
                    <div class="msg-info-name">${name}</div>
                    <div class="msg-info-time" title="${formatDay(val.createdAt)} ${formatDate(val.createdAt)}">${formatDate(val.createdAt)}</div>
                    </div>
                    ${contentElm}
                </div>
            </div>
        </div>`;
    if(val.type == typeMsgENUM.INIT) {
        msgHTML = `<div class="msg-init-room">${val.senderID == USER_INFO.userID ? 'Bạn' : val.senderName} đã khởi tạo cuộc trò chuyện</div>`
    } else if(val.type == typeMsgENUM.RENAME_ROOM) {
        msgHTML = `<div class="msg-rename-room">${val.senderID == USER_INFO.userID ? 'Bạn' : val.senderName} đã đổi tên thành ${val.content}</div>`
    }
    if(returnValue) return msgHTML;
    else {
        msgerChat.insertAdjacentHTML("beforeend", msgHTML);
        msgerChat.scrollTop += 500;
        updateInfoRoom(val);
    }
}

function renderRooms(rooms, isRenderListRoomSearch = false) {
    let elm = '';
    rooms.forEach(r => {
        let receiver = r.users.filter(u => u.id != USER_INFO.userID);
        let isOnline = receiver.find(u => u.isOnline);
        if(r.isGroup) {
            elm += `<div class="room-item" onclick="handleClickRoom(${r.roomID})" id="room-id-${r.roomID}">
                    <div class="info">
                        <div class="status">${isOnline ? 'online': 'offline'}</div>
                        <div class="name">${r.name}</div>
                        ${r.lastMsgType == typeMsgENUM.INIT ?
                            '<div class="last-msg">' + (r.lastMsgSender.userID == USER_INFO.userID ? 'Bạn' : r.lastMsgSender.userName) + ' đã khởi tạo cuộc trò chuyện</div>'
                            :
                            r.lastMsgType == typeMsgENUM.RENAME_ROOM ?
                            '<div class="last-msg">' + (r.lastMsgSender.userID == USER_INFO.userID ? 'Bạn' : r.lastMsgSender.userName) + ' đã đổi tên thành ' + r.lastMsgContent + '</div>'
                            :
                            '<div class="last-msg">' + (USER_INFO.userID == r.lastMsgSender.userID ? 'Bạn: ' : '') + r.lastMsgContent + '</div>'
                        }
                    </div>
                </div>`
        } else {
            elm += `<div class="room-item" onclick="handleClickRoom(${r.roomID})" id="room-id-${r.roomID}">
                    <div class="info">
                        <div class="status">${isOnline ? 'online': 'offline'}</div>
                        <div class="name">${receiver[0].name}</div>
                        ${r.lastMsgType == typeMsgENUM.INIT ?
                            '<div class="last-msg">' + (r.lastMsgSender.userID == USER_INFO.userID ? 'Bạn' : r.lastMsgSender.userName) + ' đã khởi tạo cuộc trò chuyện</div>'
                            :
                            r.lastMsgType == typeMsgENUM.RENAME_ROOM ?
                            '<div class="last-msg">' + (r.lastMsgSender.userID == USER_INFO.userID ? 'Bạn' : r.lastMsgSender.userName) + ' đã đổi tên thành ' + r.lastMsgContent + '</div>'
                            :
                            '<div class="last-msg">' + (USER_INFO.userID == r.lastMsgSender.userID ? 'Bạn: ' : '') + r.lastMsgContent + '</div>'
                        }
                    </div>
                </div>`
        }
        
    })
    if(isRenderListRoomSearch) {
        msgeRoomSearch.innerHTML = null;
        msgeRoomSearch.insertAdjacentHTML("beforeend", elm);
        leftsideElm.classList.add('active-room-search');
    } else {
        msgeRoom.insertAdjacentHTML("beforeend", elm);
    }
}

function updateInfoRoom(res) {
    let roomItemElm = get(`#room-id-${res.roomID}`);
    if(!roomItemElm) {
        let receiver = res.usersInfo.filter(u => u.id != USER_INFO.userID);
        let room = res.roomInfo;
        let isOnline = receiver.find(x => x.isOnline);
        let elm = null;
        if(room.isGroup) {
            elm = `<div class="room-item" onclick="handleClickRoom(${res.roomID})" id="room-id-${res.roomID}">
                <div class="info">
                    <div class="status">${isOnline ? 'online': 'offline'}</div>
                    <div class="name">${room.name}</div>
                    <div class="last-msg">${USER_INFO.userID == room.lastMsgSender.userID ? 'Bạn' : room.lastMsgSender.userName}: ${room.lastMsgContent}</div>
                </div>
            </div>`
        } else {
            elm = `<div class="room-item" onclick="handleClickRoom(${res.roomID})" id="room-id-${res.roomID}">
                    <div class="info">
                        <div class="status">${isOnline ? 'online': 'offline'}</div>
                        <div class="name">${receiver[0].name}</div>
                        <div class="last-msg">${USER_INFO.userID == room.lastMsgSender.userID ? 'Bạn: ' : ''} ${room.lastMsgContent}</div>
                    </div>
                </div>`
        }
        msgeRoom.insertAdjacentHTML("afterbegin", elm);
    } else {
        let lastMsgInfo = roomItemElm.querySelector(".last-msg");
        lastMsgInfo.innerHTML = `${USER_INFO.userID == (res.senderID || res.userID) ? 'Bạn' : res.senderName}: ${res.content}`;   
        roomItemElm.remove();
        msgeRoom.prepend(roomItemElm);
    }
}

function handleClickRoom(roomID) {
    dateArray = [];
    currentRoomInfo = allRoom.find(e => e.roomID == roomID);
    currentRoomID = roomID;
    getAndRenderMsgs(pageIndexMsg = 1, true);
    disabledCheckScrollEvent = true;
    handleClickBackToRoom();
    realClient.markReadMessage(roomID);
    handleClickInfo();
}

function handleCallAPIRoom(payload, callback) {
    realClient.getListRoom(payload, callback);
}

function handleClickBackToRoom() {
    leftsideElm.classList.remove('active-room-search');
    msgSearchInput.value = null;
}

function toLowerCaseNonAccentVietnamese(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    return str;
}

function handleKeyUpSearch(e) {
    let value = e.target.value;
    if(!value) value = "";
    value = toLowerCaseNonAccentVietnamese(value);
    let payload = {
        search: {
            columns: ["name"],
            value: value
        },
        sort: {updatedAt: -1}
    };
    handleCallAPIRoom(payload, (res) => {
        renderRooms(res.result.data, true);
    })
}

function handleUserTyping() {
    let userNameArr = Object.values(userTyping);
    let userTypingElm = null;
    if(userNameArr.length > 0) {
        userTypingElm = `<div>${userNameArr.join(", ")} typing</div>`;
    }
    msgUserTyping.innerHTML = userTypingElm;
}

function handleUserBeginTyping(user) {
    userTyping[user.id] = user.name;
    this.handleUserTyping();
}

function handleUserEndTyping(user) {
    delete userTyping[user.id];
    this.handleUserTyping();
}

function getAndRenderMsgs(pageIndex, isResetMsg) {
    let payload = {
        pageIndex,
        roomID: currentRoomID
    }
    realClient.getListMsg(payload, (res) => {
        renderMsgs(res.result.data, isResetMsg);
    })
}

function renderMsgs(data, isResetMsg = false) {
    isResetMsg && (msgerChat.innerHTML = null);
    const div = document.createElement("div");
    div.classList.add('m-b-10')
    let msgHTML = '';
    data.sort(function(a, b){return a.msgID - b.msgID}).forEach((e) => {
        msgHTML += appendMessage(e.senderName, 'https://image.flaticon.com/icons/svg/145/145867.svg', e.senderID == USER_INFO.userID ? 'right' : 'left', e);
    })
    div.innerHTML = msgHTML;
    msgerChat.insertBefore(div, msgerChat.children[0]);
    msgerChat.scrollTop += div.clientHeight;
}

function handleKeyUpInput(e) {
    //#region người dùng dừng gõ tin nhắn
    clearTimeout(typingTimeout);
    typingTimeout = setTimeout(() => {
        realClient.userEndTyping(currentRoomID);
    }, 600);
    //#endregion
    //#region Người dùng gửi tin nhắn
    if (e.key === 'Enter' || e.keyCode === 13) {
        if(!e.target.value) return;
        let data = {
            content: e.target.value,
            userID: USER_INFO.userID,
            roomID: currentRoomID,
            createdAt: new Date()
        }
        realClient.sendMessage(data, (payload) => {
            data.msgID = payload.requestID;
            appendMessage(USER_INFO.name, 'https://image.flaticon.com/icons/svg/145/145867.svg', 'right', data, false);
        })
        e.target.value = null;
    }
    //#endregion
    //#region người dùng gõ tin nhắn
    else {
        realClient.userBeginTyping(currentRoomID);        
    }
    //#endregion
}

function handleCancelCall() {
    realClient.cancelAnswer(currentRoomID);
    msgeCallPopup.classList.remove('d-block');
    msgeWaitCallPopup.classList.remove("d-block");
    msgeWaitCallPopup.querySelector('.content').innerHTML = "Calling...";
    document.getElementById('video-grid').classList.remove('d-flex');
    document.getElementById('call-audio').classList.remove('d-block');
    document.getElementById('video-grid').querySelectorAll('video').forEach(e => e.remove())
    clearInterval(intervalTimeCall);
}

function handleAgreeCall() {
    realClient.aggreeAnswer(currentRoomID, isCallVideo);
    msgeCallPopup.classList.remove('d-block');
    if(!isCallVideo) {
        document.getElementById('call-audio').classList.add('d-block');
        document.getElementById('call-audio').querySelector('.call-audio-name').innerHTML = `<b>${currentRoomInfo.name}</b>`;
    } else {
        document.getElementById('video-grid').classList.add('d-flex');
    }
    countCallTime();
}

function checkCurrentRoomToRender(roomID) {
    return currentRoomID == roomID;
}

function handleScrollMsg(e) {
    if(e.scrollTop == 0 && !disabledCheckScrollEvent) {
        pageIndexMsg++;
        getAndRenderMsgs(pageIndexMsg);
    } else if(disabledCheckScrollEvent) {
        disabledCheckScrollEvent = false;
    }
}

function handleHoverMsg(e, senderID) {
    if(!e.getAttribute("readBy")) return;
    try {
        let readBy = JSON.parse(e.getAttribute("readBy"));
        genTitleUserReadMsg(e, readBy, senderID);
    } catch {

    }
}

function genTitleUserReadMsg(e, readBy, senderID) {
    if(!readBy) return;
    let readUserNameArr = [];
    readBy.forEach(r => {
        let readUser = currentRoomInfo.users.find(x => x.id == r.readByUserID && x.id != USER_INFO.userID && x.id != senderID);
        readUser && readUserNameArr.push(readUser.name);
    })
    readUserNameArr.length > 0 && e.setAttribute("title", readUserNameArr.join(", ") + ' đã đọc');
}

function countCallTime() {
    var timeCall = 0;
    intervalTimeCall = setInterval(() => {
        timeCall++;
        document.getElementById('video-area').querySelector('.call-status').innerHTML = `<b>${formatCallTime(timeCall)}</b>`;
    }, 1000);
}

function formatCallTime(time) {
    var days = Math.floor(time / (60 * 60 * 24));
    days = days > 0 ? days + "d " : "";
    var hours = Math.floor((time % (60 * 60 * 24)) / (60 * 60));
    hours = hours > 0 ? hours + "h " : "";
    var minutes = Math.floor((time % ( 60 * 60)) / (60));
    minutes = minutes > 0 ? minutes + "m " : "";
    var seconds = Math.floor((time % (60)));
    seconds = seconds > 0 ? seconds + "s " : "";
    return days + hours + minutes + seconds;
}

function handleMuteAudio() {
    realClient.toggleMuteAudio();
}

function handleMuteVideo() {
    document.getElementById('call-audio').classList.remove('d-block');
    document.getElementById('video-grid').classList.add('d-flex');
    realClient.toggleMuteVideo();
}

function handleShareScreen() {
    realClient.shareScreen(currentRoomID);
}

function renderSelectedFiles(files) {
    let msgFilesElm = $(".msger-files");
    filesInChat = Array.from(files);
    for (let i = 0; i < files.length; i++) {
        files[i].id = i;
        msgFilesElm.prepend(`
                <div class="msg-file" id=${files[i].id} href="${files[i].name.toLocaleLowerCase()}" file_extension="${getFileExtesion(files[i].name)}">
                    <div class="file-icon"></div>
                    <div class="file-name">${files[i].name}</div>
                    <button class="delete-file-btn"><div class="icon"></div></button>
                </div>`);
    }
}

function getFileExtesion(fileName) {
    fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));
    return fileExtension.toLocaleLowerCase();
}

function initEvents() {
    $(document).on('change', '#uploadFile', function (e) {
        let files = e.target.files;
        renderSelectedFiles(files);
    })
    $(document).on('click', '.msg-file .delete-file-btn', function (e) {
        e.stopPropagation();
        let fileId = $(this).parents(".msg-file").attr("id");
        let index = filesInChat.findIndex(c => c.id == fileId);
        filesInChat = filesInChat.slice(index, 1);
        $(this).parents(".msg-file").remove();
    });
    document.addEventListener('click', function handleClickOutsideBox(event) {
        if(!document.querySelector('.more-feature-btn') || document.querySelector('.more-feature-btn').contains(event.target)) return;
        if (!dropdownElm.contains(event.target)) {
            dropdownElm.style.display = 'none';
        }
      });
}

lstUserSelected = [];
checkActiveCreateRoomFeature = true;

/**
 * Hàm show popup tạo room 1-n
 * created by pdthien 16.10.2022
 */
function handleShowCreateRoomPopup(isCreateRoom = true) {
    checkActiveCreateRoomFeature = isCreateRoom;
    listUsersPopup.classList.remove('d-none');
    realClient.getListUser(null, function (res) {
        let elm = "";
        res.result.data.filter(x => x.userID != USER_INFO.userID).forEach((e) => {
            elm += `
                <div class="d-flex">
                    <input type="checkbox" value=${e.userID} name="user" class="user-checkbox">
                    <div>${e.name}</div>
                </div>
            `
        })
        listUsersElm.innerHTML = elm;
        let allCheckBox = document.querySelectorAll('.user-checkbox')

        allCheckBox.forEach((checkbox) => { 
        checkbox.addEventListener('change', (event) => {
            let checked = event.target.checked;
            let value = parseInt(event.target.value);
            if (checked) {
                lstUserSelected.push(value);
            } else {
                lstUserSelected.splice(lstUserSelected.findIndex(x => x == value) , 1);
            }
            console.log(lstUserSelected)
        })
        })
    })
}

/**
 * Hàm huỷ tạo room
 * created by pdthien 16.10.2022
 */
function handleCancelCreateRoom() {
    listUsersPopup.classList.add('d-none');
}

/**
 * Hàm xử lý chat 1-1
 */
function handleChatOne(userID) {
    realClient.getRoomByUserID(userID, (data) => {
        let res = data.result
        if(res.type == 1) {
            updateInfoRoom(res);
            !allRoom.find(x => x.roomID == res.roomID) && allRoom.push(res.roomInfo);
            handleClickRoom(res.roomID);
        } else {
            let roomInfo = res.roomInfo;
            !allRoom.find(x => x.roomID == roomInfo.roomID) && allRoom.push(roomInfo);
            handleClickRoom(roomInfo.roomID);
        }
    });
}

/**
 * Hàm xử lý tạo mới room 1-n
 * created by pdthien 16.10.2022
 */
function handleCreateNewRoomOrAddMember() {
    if(checkActiveCreateRoomFeature) {
        lstUserSelected.length > 0 && realClient.createRoomGroup(lstUserSelected, (data) => {
            let res = data.result
            updateInfoRoom(res);
            !allRoom.find(x => x.roomID == res.roomID) && allRoom.push(res.roomInfo);
            if(!checkCurrentRoomToRender(res.roomID)) return;
            let position = "left";
            if(res.senderID == USER_INFO.userID) position = "right";
            appendMessage(res.senderName, 'https://image.flaticon.com/icons/svg/145/145867.svg', position, res, false);
        });
    } else {
        realClient.addUserInRoom(currentRoomID, lstUserSelected, (res) => {
            currentRoomInfo.users.push(...res.result.data);
            handleClickInfo();
        })
    }
    handleCancelCreateRoom();
}

function handleClickInfo() {
    let elm = `
    <div class="d-flex room-name-root">
        ${
            currentRoomInfo.createdByUserID == USER_INFO.userID ?
            '<i class="fa fa-user-plus" onclick="handleShowCreateRoomPopup(false)"></i>'
            : ''
        }
        <div class="room-name" contenteditable="true" onblur="handleEditNameRoom()">
            ${currentRoomInfo.isGroup ? currentRoomInfo.name : currentRoomInfo.users.find(u => u.id != USER_INFO.userID).name}
        </div>
    </div>
        <div class="room-paricipate">
            ${currentRoomInfo.users.map(u => {
                return `<div class="d-flex"><div class="user-name"> ${u.name} </div><div onclick="openMoreFeatureRoom('${u.id}')" class="more-feature-btn"><i class="fa fa-ellipsis-h"></i></div></div>`
            }).join('')}
        </div>
    `;
    roomInfoElm.innerHTML = elm;
}

function openMoreFeatureRoom(userID) {
    event.stopPropagation();
    let target = event.target;
    setDropdown(target);
    let eml = currentRoomInfo.createdByUserID == USER_INFO.userID ?
    `<div class="item-feature" onclick="handleRemoveUser(${userID})">Xoá khỏi nhóm</div>
                            <div class="item-feature" onclick="handleChatOne(${userID})">Nhắn tin</div>`
    : `<div class="item-feature" onclick="handleChatOne(${userID})">Nhắn tin</div>`;
    dropdownElm.innerHTML = eml;
}

function openMoreFeatureMsg(msgID, senderID) {
    event.stopPropagation();
    let target = event.target;
    setDropdown(target, 200);
    let eml = senderID == USER_INFO.userID ?
    `<div class="item-feature" onclick="handleDeleteMsg(${msgID})">Xoá tin nhắn</div>
     <div class="item-feature" onclick="handleUndoMsg(${msgID})">Thu hồi tin nhắn</div>`
    : `<div class="item-feature" onclick="handleDeleteMsg(${msgID})">Xoá tin nhắn</div>`
    dropdownElm.innerHTML = eml;
}

function setDropdown(elm, width = 300, height = 'auto') {
    let properties = elm.getBoundingClientRect();
    dropdownElm.style.top = properties.top + 20;
    let leftOutside = (properties.left + width - window.outerWidth)
    dropdownElm.style.left = properties.left - (leftOutside > 0 ? leftOutside : 0) - 6;
    dropdownElm.style.width = width;
    dropdownElm.style.height = height;
    dropdownElm.style.display = 'block';
}

function handleRemoveUser(userID) {
    realClient.deleteUserInRoom(currentRoomID, userID, (res) => {
        if(res.success) {
            currentRoomInfo.users.splice(currentRoomInfo.users.findIndex(u => u.id == userID), 1);
            dropdownElm.style.display = 'none';
            handleClickInfo();
        }
    });
}

function handleEditNameRoom() {
    let newName = event.target.innerText;
    if(currentRoomInfo.name != newName) {
        realClient.editRoomName(currentRoomID, newName);
        msgHTML = `<div class="msg-rename-room">Bạn đã đổi tên thành ${newName}</div>`;
        msgerChat.insertAdjacentHTML("beforeend", msgHTML);
        msgerChat.scrollTop += 500;
        updateInfoRoom({roomID: currentRoomID, senderID: USER_INFO.userID, content: `Bạn đã đổi tên thành ${newName}`});
    }
}

function handleDeleteMsg(msgID) {
    realClient.deleteMsg(currentRoomID, msgID, function(res) {
        get(`#msg-id-${msgID}`).remove();
        dropdownElm.style.display = 'none';
    });
}

function handleUndoMsg(msgID) {
    realClient.undoMsg(currentRoomID, msgID, function(res) {
        get(`#msg-id-${msgID}`).remove();
        dropdownElm.style.display = 'none';
    });
}

$(document).ready(function () {
    initEvents();
});