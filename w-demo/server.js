//#region import library
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const helmet = require("helmet");
const morgan = require('morgan');
//#endregion import library

//#region app
const app = express();
app.use(
  helmet({
    contentSecurityPolicy: false
 })
);
app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/public/view"));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());
app.use(morgan("dev"));
//#endregion app

//#region server
const server = http.createServer(app);
const PORT = process.argv[2] || process.env.PORT || 5050;
server.listen(PORT, () => console.log(`running on ${PORT}`))
//#endregion server

const axios = require('axios');
const jwt = require('jsonwebtoken');

const registerServerChatAPI = "https://pdthien.store:3030/api/v1/auth/register";
const tenantID = "4a7bb0b5-e7f2-4688-a0af-eae7a08226be";
const keySecret = "19ceb948-cd85-4e51-a485-c1d1cd8c0be4";

async function createToken(req, res, next) {
  let { name, mail, password } = req.body;
  let accessToken = jwt.sign({ tenantID }, keySecret, { expiresIn: '12h' });
  const config = {
    headers: {
      Authorization: 'Bearer ' + accessToken
    }
  };
  axios.post(registerServerChatAPI, { name, mail, password }, config)
    .then(response => {
      res.status(200).json(response.data);
    })
    .catch(error => {
      res.status(500).json({error});
    });
}
const router = express.Router();
router.post('/register', createToken);
app.use('/api/v1', router);