//#region import library
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const cors = require('cors');
// const helmet = require("helmet");
//#endregion import library

//#region import file
require('./server/global');

//#region app
const app = express();
// app.use(
//   helmet({
//     contentSecurityPolicy: false
//  })
// );
app.use(cors());
app.use(bodyParser.json());
dotenv.config();
//#endregion app

//#region mongdoDB
mongoose.connect(process.env.URL_MONGDO_DB || "mongodb://mongo:27017/chat-real", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

mongoose.connection.on('connected', () => {
  console.log('Mongo has connected succesfully')
})
mongoose.connection.on('reconnected', () => {
  console.log('Mongo has reconnected')
})
mongoose.connection.on('error', error => {
  console.log('Mongo connection has an error', error)
  mongoose.disconnect()
})
mongoose.connection.on('disconnected', () => {
  console.log('Mongo connection is disconnected')
})
//#endregion mongdoDB

//#region server
const server = http.createServer(app);
const PORT = process.env.PORT || 4000;
server.listen(PORT, () => console.log(`running on ${PORT}`))

const fileRouter = require('./server/routers/FileRouter');
app.use('/api/v1/file', fileRouter);

// handle error
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  const statusCode = err.status || 500;
  return res.status(statusCode).json({
      status: 'error',
      code: statusCode,
      message: err.message || 'Internal Server Error'
  });
});
//#endregion server
