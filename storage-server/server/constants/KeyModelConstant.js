module.exports = {
    USER_MODEL: 'userID',
    ROOM_MODEL: 'roomID',
    MESSAGE_MODEL: 'msgID',
    ACCESS_KEY_MODEL: 'tenantID'
}