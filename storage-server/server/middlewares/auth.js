const jwt = require('jsonwebtoken');
const AccessKeyModel = require('../models/AccessKeyModel');
const roomModel = require('../models/RoomModel');
const messageModel = require('../models/MessageModel');

function parseJwt(token) {
    var base64Url = token.split('.')[1];
    if (!base64Url) return null;
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(jsonPayload);
}

/**
 * Hàm kiểm tra authen
 * created by pdthien 01.07.2022
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns 
 */
async function handleCheckAuthen(req, res, next) {
    let query = req.query;
    let roomID = query.roomID;
    let msgID = query.msgID;
    let accessToken = query.accessToken;
    let decodeToken = await decodeAccessToken(accessToken);
    let userID = decodeToken.userID;
    let tenantID = decodeToken.tenantID;
    if (!roomID || !userID || !msgID) {
        $responseAPI.onError(res, 'UNAUTHEN', 401);
        return;
    }
    let checkFileAccess = await handleCheckFileAccess(roomID, msgID, userID, tenantID);
    if (!checkFileAccess) {
        $responseAPI.onError(res, 'not found', 403);
        return;
    }
    next();
}

/**
 * Hàm kiểm tra có quyền truy cập file
 * created by pdthien 01.07.2022
 * @param {*} roomID 
 * @param {*} msgID 
 * @param {*} userID 
 * @returns 
 */
async function handleCheckFileAccess(roomID, msgID, userID, tenantID) {
    let room = await roomModel.findOne({ roomID, tenantID }).lean();
    if (!room) return false;
    let participantsRoom = room.userIDs;
    let checkUser = participantsRoom.includes(userID);
    if (!checkUser) return false;
    let message = await messageModel.findOne({ msgID, roomID, tenantID }).lean();
    if (!message) return false;
    return true;
}

/**
     * Hàm verity jwt
     * created by pdthien 19.04.2022
     * @param {*} req 
     * @param {*} res 
     */
async function decodeAccessToken(token) {
    if (!token) {
        return false;
    }
    try {
        console.log(token)
        let parseJwtObject = parseJwt(token);
        console.log(parseJwtObject)

        let tenantID = parseJwtObject.tenantID;
        let accessKey = await AccessKeyModel.findOne({ tenantID });
        console.log(accessKey)

        let result = jwt.verify(token, accessKey.keySecret);
        return result;
    } catch (err) {
        return false;
    }
}
/**
 * Hàm verity jwt
 * created by pdthien 19.04.2022
 * @param {*} req 
 * @param {*} res 
 */
async function verifyAccessToken(req, res, next) {
    let authorizationHeader = req.headers['authorization'] || 'Bear ' + req.query.accessToken;
    if (!authorizationHeader) {
        $responseAPI.onError(res, 'UNAUTHEN', 401);
        return;
    }
    let token = authorizationHeader.split(' ')[1];
    if (!token) {
        $responseAPI.onError(res, 'UNAUTHEN', 401);
        return;
    }
    let parseJwtObject = parseJwt(token);
    if (!parseJwtObject) return $responseAPI.onError(res, 'UNAUTHEN', 401);
    let tenantID = parseJwtObject.tenantID;
    let accessKey = await AccessKeyModel.findOne({ tenantID });
    jwt.verify(token, accessKey.keySecret, (err, data) => {
        if (err) {
            $responseAPI.onError(res, 'UNPERMISSION', 403);
            return;
        }
        next();
    });
}
module.exports = {
    decodeAccessToken,
    verifyAccessToken,
    handleCheckAuthen
}