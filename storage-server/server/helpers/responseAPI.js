module.exports = {
    onSuccess: (res, message, data) => {
        const resdata = {
            status: 200,
            success: true
        };
        if(message) resdata.message = message;
        if(data) resdata.result = data;
        return res.status(200).json(resdata);
    },
    onError: (res, message, status) => {
        const resdata = {
            status: status || 500,
            success: false,
            message: String(message)
        }
        return res.status(500).json(resdata);
    },
    validationError: (res, message, data) => {
        const resdata = {
            status: 400,
            success: false,
            message: String(message)
        }
        if(data) resdata.result = data;
        return res.status(400).json(resdata)
    }
}