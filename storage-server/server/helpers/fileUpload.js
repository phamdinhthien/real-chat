const multer = require('multer');
const fs = require('fs');
const Minio = require('minio');

var minioClient = new Minio.Client({
    endPoint: process.env.END_POINT_MINIO,
    port: 9000,
    useSSL: false,
    accessKey: 'QaQHClAWbCyZChZS',
    secretKey: 'PZVJQMtG7k0IAfGDE3YxYNG0pwgCdVHl'
});

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        var dir = './uploads';
        callback(null, dir);
    },
    filename: function (req, file, callback) {
        callback(null, formatDate(new Date()) + '-' + file.originalname.replace(/[&=]/g, "-") + '-' + Date.now());
    }
});
var upload = multer({storage: storage}).array('files', 12);
/**
 * Hàm format date
 * @param {*} date 
 * @returns 
 */
function formatDate(date) {
    const day = date.getDate().toString().padStart(2, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const year = date.getFullYear().toString()
    const formattedDate = `${year}.${month}.${day}`
    return formattedDate
}
/**
 * Hàm xử lý lưu file tới minio
 * @param {*} param0 
 */
function saveFileToMinio({file, currentDate}) {
    var metaData = {
        'Content-Type': 'application/octet-stream'
    }
    minioClient.fPutObject(currentDate, file.filename, file.path, metaData, function (err, etag) {
        if (err) return console.log(err)
        fs.unlink(file.path, function (err) {
            if (err) {
              console.error(err);
              return;
            }
          });
    });
}
async function uploadFile(req, res, next) {
    upload(req, res, function (err) {
        if (err) {
            return $responseAPI.onError(res, 'something wrong happened');
        }
        minioClient.listBuckets(function (err, buckets) {
            if (err) {
                console.log('Error: ', err)
            } else {
                let file = req.files[0];
                let currentDate = formatDate(new Date());
                if (buckets.length == 0 || !buckets.find(x  => x.name == currentDate)) {
                    minioClient.makeBucket(currentDate, 'us-east-1', function (err) {
                        if (err) {
                            console.log(err)
                        } else {
                            console.log('Bucket created successfully in "us-east-1".')
                        }
                        saveFileToMinio({file, currentDate});
                    });
                } else {
                    saveFileToMinio({file, currentDate});
                }
            }
        })
        return $responseAPI.onSuccess(res, null, req.files);
    })
}
/**
 * Hàm lấy file
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function getFile(req, res, next) {
    let query = req.query;
    let pathFile = query.path;
    minioClient.getObject(pathFile.split('-')[0], pathFile, function(err, dataStream) {
        if (err) {
            console.log(err);
          return $responseAPI.onError(res, 'something wrong happened');
        }
        dataStream.pipe(res);
    });
}

module.exports = {
    uploadFile,
    getFile
};