
const idName = _keyModelConstant.MESSAGE_MODEL;
var readBySchema = new Schema({
    _id: false,
    readByUserID: Number,
    readAt: {
        type: Date,
        default: Date.now()
    }
});

var messageSchema = extendBaseSchema(idName, {
    msgID: {
        type: Number,
        unique: true
    },
    roomID: {
        type: Number,
        required: true
    },
    senderID: {
        type: Number,
        required: true
    },
    senderName: {
        type: String,
        required: true
    },
    type: {
        type: Number,
        required: true
    },
    state: {
        type: Number,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    readBy: {
        type: Array,
        default: [readBySchema]
    },
    requestID: {
        type: String,
        required: true
    },
    tenantID: {
        type: String
    }
});

messageSchema.statics.markReadMessage = async function (roomID, currentUserID) {
    try {
        await this.updateMany(
            {
              roomID: roomID, 
              'readBy.readByUserID': { $ne: currentUserID }
            },
            {
              $addToSet: {
                readBy: { readByUserID: currentUserID }
              }
            },
            {
              multi: true
            }
        );
    } catch (error) {
      throw error;
    }
}

messageSchema.plugin(AutoIncrement, {inc_field: idName});
var Messages = mongoose.model('Messages', messageSchema);

module.exports = Messages;