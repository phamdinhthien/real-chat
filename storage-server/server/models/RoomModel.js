
const idName = _keyModelConstant.ROOM_MODEL;
var roomSchema = extendBaseSchema(idName, {
    roomID: {
        type: Number,
        unique: true
    },
    name: {
        type: String,
        required: true,
        minlength: 1
    },
    isGroup: Boolean,
    userIDs: {
        type: [Number],
        required: true
    },
    createdByUserID: {
        type: Number,
        required: true
    },
    createdByUserName: {
        type: String,
        required: true
    },
    lastMsgID: Number,
    lastMsgContent: String,
    lastMsgType: Number,
    lastMsgSender: Object,
    tenantID: {
        type: String
    }
});

roomSchema.plugin(AutoIncrement, {inc_field: idName});
var Rooms = mongoose.model('Rooms', roomSchema);

module.exports = Rooms;