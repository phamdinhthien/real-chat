
module.exports = function(model, idName) {
    /**
     * Hàm lấy bản ghi theo ID
     * created by pdthien 21.05.2022
     * @param {*} ID 
     * @returns 
     */
    model.statics.getByID = async function (ID, selectColumns = null) {
        try {
            let query = {};
            query[idName] = ID;
            const result = await this.findOne(query).select(selectColumns).lean();
            if (!result) return null;
            return result;
        } catch (error) {
          throw error;
        }
    }
    /**
     * Hàm lấy danh sách bản ghi theo id
     * created by pdthien 21.05.2022
     * @param {*} IDs 
     * @returns 
     */
    model.statics.getListByIDs = async function (IDs, selectColumns = null) {
        try {
            let query = {};
            query[idName] = {$in: IDs};
            const result = await this.find(query).select(selectColumns);
            if (!result) throw ({ error: 'No record with this id found' });
            return result;
        } catch (error) {
            throw error;
        }
    }
};

