const express = require('express');
const router = express.Router();

const {uploadFile, getFile} = require('../helpers/fileUpload.js');

const authMiddleware = require('../middlewares/auth');

const asyncHandler = (fn) => {
    return (req, res, next) => {
        fn(req, res, next).catch(next);
    }
}

router.post('/upload-file', asyncHandler(authMiddleware.verifyAccessToken), asyncHandler(uploadFile));
router.get('/get-file', asyncHandler(authMiddleware.verifyAccessToken), asyncHandler(authMiddleware.handleCheckAuthen), asyncHandler(getFile));

module.exports = router;

