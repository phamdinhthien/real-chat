
global.mongoose = require('mongoose');
global.Schema = mongoose.Schema;

const BaseModel = require('./models/BaseModel');
global.AutoIncrement = require('mongoose-sequence')(mongoose);
global.extendBaseSchema = function(IDName, definition) {
    let res = new Schema({
      ...definition},
      {timestamps: true }
    );
    BaseModel(res, IDName);
    return res;
}

global._keyModelConstant = require('./constants/KeyModelConstant');
global.$responseAPI = require('./helpers/responseAPI');
